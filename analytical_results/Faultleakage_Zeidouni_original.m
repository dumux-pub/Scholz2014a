function Faultleakage

% By: Mehdi Zeidouni
% Zeidouni, M., (2012), Analytical model of leakage through fault to overlying formations, Water Resources Research, 48, W00N02. 
% January 14, 2014

clc
clear all
global N au TD etaD

%User input
q=0.005;   %injection rate (m3/s)
aa=500;  %well-fault distance (m)
eta=10;   %Diffusivity coefficient of the injection zone (m2/s)
k=1e-13; %Injection zone permeability (m2)
h=60;    %Injection zone thickness (m)
mu=1e-3; %fluid viscosity (Pa.s)

N=15; 
etau=10*ones(N,1);  %Upper aquifers diffusivities (m2/s)
ku=1e-13*ones(N,1);  %Upper aquifers permeabilities (m2)
hu=40*ones(N,1);  %Upper aquifers thicknesses (m)
kf=1e-14*ones(N,1);  %Upfault permeability (m2)
wf=1.68*ones(N,1);  %fault width (m)
hcaprock=30*ones(N,1);  %Cap-rock thickness (m) 
tm=(86400*365)*[0.001 0.01 0.1 1 10 100];  %times at which leakage rate to be evaluated (s)

%definitions
tDm=eta*tm/aa^2;
for ii=1:N
    if ii==1
        L(ii)=(h+hu(ii))/2+hcaprock(ii);
    else
        L(ii)=(hu(ii)+hu(ii-1))/2+hcaprock(ii);
    end
end
au=aa*kf.*wf./(2*k*h*L');
TD=(ku.*hu)./(k.*h);
etaD=etau/eta;

qlD=RateSim(tDm);

figure;semilogx(tm/86400/365,qlD)
xlabel('Time (yr)')
ylabel('q_l_D')

function qlD=RateSim(tDm)

global xD w au N TD
xD=0;
Fs = 1044; % sampling frequency
NN = 1024*8*8; % number of samples data

k=[0:NN/2-1 -NN/2:-1]; % N even

w = k/Fs; % w vector, max(w) should be a range beyond which qD is zero

qlD=zeros(N,length(tDm));

for jj=1:length(tDm)
    data_w=(1/(2*pi))*gavstehmatrix('qD',tDm(jj),16,length(w),N+2);
    data_f = fft(data_w);
    data_f = (data_f)./Fs;
    for kk=1:(N+2)
        data_f(:,kk)=abs(fftshift(data_f(:,kk)));
    end
    for kk=N+2:-1:3
        for ii=2:NN
%             qlD(kk-2,jj)=qlD(kk-2,jj)+2*au(kk-2)*((data_f(ii,kk-1)-data_f(ii,kk)+data_f(ii-1,kk-1)-data_f(ii,kk)))*(2*pi*Fs/NN)/2;
            qlD(kk-2,jj)=qlD(kk-2,jj)+2*TD(kk-2)*(data_f(ii,kk)+data_f(ii-1,kk))*(2*pi*Fs/NN)/2;
            
        end
    end
end

function f=qD(s)
global xD au TD etaD w N
f=zeros(length(w),N+2);

for ii=1:length(w)
    A= sqrt(w(ii)^2+s);
    for kk=1:N
        Au(kk)=sqrt(w(ii)^2+s/etaD(kk));
    end
    
    C0=exp(-A)/(2*s*A);
    H=zeros(N+2,N+2);
    
    H(1,1)=-1;
    H(1,2)=1;
    H(2,1)=A;
    H(2,2)=A+2*au(1);
    H(2,3)=-2*au(1);
    
    for kk=3:(N+2)
        for ll=(kk-1):(N+2)
            if ll==(kk-1)
                H(kk,ll)=-au(ll-1);
            elseif ll==kk
                H(kk,ll)=au(ll-2)+TD(ll-2)*Au(ll-2);
            else
                H(kk,ll)=TD(ll-2)*Au(ll-2);
            end
        end
    end
    
    F=zeros(N+2,1);
    F(1)=C0;
    F(2)=C0*A;
    
    C=inv(H)*F;
    
    ff=zeros(1,N+2);
    for kk=1:(N+2)
        if kk==1
            ff(kk)=(1./(2*s*A)).*exp(-A)+C(1);
        elseif kk==2
            ff(kk)=C(2);
        else
            ff(kk)=C(kk).*Au(kk-2);
        end
    end
    f(ii,:)=ff;
end


function ilt=gavstehmatrix(funname,t,L,matlength,matwidth)
nn2 = L/2;
nn21= nn2+1;

for n = 1:L
    z = 0.0;
	for k = floor( ( n + 1 ) / 2 ):min(n,nn2)
        z = z + ((k^nn2)*factorial(2*k))/ ...
            (factorial(nn2-k)*factorial(k)*factorial(k-1)* ...
            factorial(n-k)*factorial(2*k - n));
    end
    v(n)=(-1)^(n+nn2)*z;
end

sum = zeros(matlength,matwidth);
ln2_on_t = log(2.0) / t;
for n = 1:L
    p = n * ln2_on_t;
    sum = sum + v(n) .* feval(funname,p);
end 
	ilt = sum .* ln2_on_t;
    



