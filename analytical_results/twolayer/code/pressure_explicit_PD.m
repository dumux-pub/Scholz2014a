function Faultleakage

% By: Mehdi Zeidouni
% Zeidouni, M., (2012), Analytical model of leakage through fault to overlying formations, Water Resources Research, 48, W00N02.
% January 14, 2014
%Edited by: Simon Scholz 


clc
clear all
global N a au TD etaD xDa PD distanceIter hd

%User input
q=0.005;    %injection rate (m3/s)
aa=100;     %well-fault distance (m)
eta=10;     %Diffusivity coefficient of the injection zone (m2/s)
k=1e-11;    %Injection zone permeability (m2)
h=20;       %Injection zone thickness (m)
mu=1e-3;    %fluid viscosity (Pa.s)

N=1; %Number of overlying aquifers
etau=10*ones(N,1);   %Upper aquifers diffusivities (m2/s)
ku=1e-11*ones(N,1);  %Upper aquifers permeabilities (m2)
hu=20*ones(N,1);     %Upper aquifers thicknesses (m)
kf=1.1905-10*ones(N,1); %1.68e-13*ones(N,1);  %Upfault permeability (m2)
wf=1.68*ones(N,1);      %fault width (m)
hcaprock=30*ones(N,1);  %Cap-rock thickness (m)
tm=36000;               %times at which leakage rate to be evaluated (s)
x=[-100:10:200];        %x values where the pressure is evaluated
assignin('base','x', x);
%definitions
tDm=eta*tm/aa^2;
for aquiferIter=1:N
    if aquiferIter==1
        L(aquiferIter)=(h+hu(aquiferIter))/2+hcaprock(aquiferIter); %length of flow path for injection zone
    else
        L(aquiferIter)=(hu(aquiferIter)+hu(aquiferIter-1))/2+hcaprock(aquiferIter); %length of flow path for upper aquifers
    end
end

a=1     %or use a=kf.*aa./(wf.*k);
hd=hu./h;
au=1    %or use au=aa*kf.*wf./(2*k*h*L');
TD=1    %or use TD=(ku.*hu)./(k.*h);
etaD=etau/eta;


PD=zeros(N+2,length(x));
for distanceIter=1:length(x)    %loop over distance for pressure evaluation
    xDa=x(distanceIter)/aa      %creating global xDa for f=qD function
   qlD=RateSim(tDm);
end

DeltaP=(q*mu.*PD./(1000*k*h))

%Plot Pressure
figure;
hold on
plot(x(11:31),DeltaP(1,11:31),x(1:11),DeltaP(2,1:11)); %plot pressure 1=injection side 2=no injection 3=upper aquifer 
xlabel('x(m)')
ylabel('\Delta P (kPa)')
title('\alpha_u=1 T_D = 1 \alpha =1') 
hold off
figure; plot(x,DeltaP(3,:));
xlabel('x(m)')
ylabel('\Delta P (kPa)')
title('\alpha_u=1 T_D = 1 \alpha =1')
         
        
     function qlD=RateSim(tDm)
        
        global xD w N TD distanceIter PD q mu k h
        %xD=0;
        Fs = 6; % inverse sampling frequency
        NN = 320; % number of sample points for y
        k=[0:NN/2-1 -NN/2:-1]; % N needs to be even - dimensionless increment in y
        w = k/Fs; % omega vector, max(w) should be a range beyond which qD is zero
        assignin('base','w', w);
        
        % dimensionless flux vector q/q_injection
        qlD=zeros(N,length(tDm));
        
      
           
        for timeIter=1:length(tDm) % loop over time steps
            % Calculate L^-1(F(p)), i.e the inverse Laplace transform to
            % obtain the Fourier transformed pressure
            data_w=gavstehmatrix('qD',tDm(timeIter),16,length(w),N+2); % Fourier transformed pressures at xd = 0, i.e. fault zone
            % Calculate F^-1(p) to obtain the pressure in the space time
            % domain. Divide by 2pi, see paragraph 52 in paper.
            data_f = (1/(2*pi))*fft(data_w);
            % Divide the pressure by the inverse sampling frequency
            data_f = (data_f)./Fs;
            % Shift the data, see paragraph 53 in paper.
            for aquiferIter=1:(N+2)
                data_f(:,aquiferIter)=abs(fftshift(data_f(:,aquiferIter)));
            end
            assignin('base','data_f',data_f);
            PD(:,distanceIter)=max(data_f);
            assignin('base','PD', PD);
        end
     
            for aquiferIter=N+2:-1:3 % loop over aquifers
                for sampleIter=2:NN % loop over y sampling points
                    % see Equation 26 in paper
%                       qlD(aquiferIter-2,timeIter)=qlD(aquiferIter-2,timeIter) ...
%                       +2*au(aquiferIter-2)*((data_f(sampleIter,aquiferIter-1)-data_f(sampleIter,aquiferIter) ...
%                       +data_f(sampleIter-1,aquiferIter-1)- ...
%                       data_f(sampleIter,aquiferIter)))*(2*pi*Fs/NN)/2;
                  % see Equation 27 in paper
                    qlD(aquiferIter-2,timeIter)=qlD(aquiferIter-2,timeIter)+ ...
                        2*TD(aquiferIter-2)*(data_f(sampleIter,aquiferIter)+ ...
                        data_f(sampleIter-1,aquiferIter))*(2*pi*Fs/NN)/2;
                    dyD=2*pi*Fs/NN
                    %(2*pi*Fs/NN) = dyD i.e dimensionless incremantion in y
                    %direction
                end
            end
 
% qD calculates the transformed pressures in the Laplace-Fourier domain
% at the fault zone in each aquifer.
% s - Laplace parameter s
% f - vector of transformed pressures at the fault zone for different
% values of omega (vector w)

    function f=qD(s)
        global xD au TD etaD w N xDa a hd
        f=zeros(length(w),N+2);
        
        for omegaIter=1:length(w)
            A= sqrt(w(omegaIter)^2+s);
            for aquiferIter=1:N
                Au=sqrt(w(omegaIter)^2+s/etaD(aquiferIter));
            end
         
            ff=zeros(1,N+2);
            for aquiferIter=1:(N+2) %iterate over aquifers
                if aquiferIter==1
                    ff(aquiferIter)=1./(2*s*A)*...
                (exp(-A*abs(xDa-1))+(A^2 ...
                *(au+Au*TD)*(au+Au*TD+2*a*hd)...
                -2*a*au*Au*TD*(au+2*a*hd+au*hd+Au*TD)...
                -au*Au*TD*au)/...
                ((A*au+A*Au*TD+au*Au*TD)...
                *(2*a*au+A*au+4*a^2*hd+2*a*A*hd...
                +2*a*au*hd+2*a*Au*TD+A*Au*TD+au*Au*TD))*...
                (exp(-A*(xDa+1))));  %Equation 17
          %  ff(aquiferIter)=(1./(2*s*A)).*exp(-abs(xDa-1)*A)+C(1)*exp(-A*xDa); 
            %Pressure
        elseif aquiferIter==2
            ff(aquiferIter)=(a./s)*((au*(au+2*a*hd+au*hd)...
                +2*Au*TD*(au+a*hd)+Au^2*TD^2)...
                /((A*au+A*Au*TD+au*Au*TD)*(2*a*au+A*au+4*a^2*hd...
                +2*a*A*hd+2*a*au*hd+2*a*Au*TD+A*Au*TD+au*Au*TD))*exp(A*(xDa-1))); %Equation 18
            % ff(aquiferIter)=C(2).*exp(A*xDa); %Pressure
        else
            limit=0; %point where Pressure ist evaluated with other function
            B=(xDa > 0);
            if any(B) 
               ff(aquiferIter)=(au./s)*((a*au+A*au+2*a^2*hd+a*A*hd+a*au*hd...
                   +a*Au*TD+A*Au*TD+au*Au*TD)...
                /((A*au+A*Au*TD+au*Au*TD)*(2*a*au+A*au+4*a^2*hd...
                +2*a*A*hd+2*a*au*hd+2*a*Au*TD+A*Au*TD+au*Au*TD))*exp(-Au*xDa-A)); %Equation 19
                %ff(aquiferIter)=-Au.*C(aquiferIter).*exp(-Au.*xDa); %Pressure derivative 
                % ff(aquiferIter)=C(aquiferIter).*exp(-Au.*xDa); %Pressure PD3
            else
               ff(aquiferIter)=(a*au./s)*((au+2*a*hd+A*hd+au*hd...
                   +Au*TD)/((A*au+A*Au*TD+au*Au*TD)*(2*a*au+A*au+4*a^2*hd...
                +2*a*A*hd+2*a*au*hd+2*a*Au*TD+A*Au*TD+au*Au*TD))*exp(Au*xDa-A)); %Equation 20
            %ff(aquiferIter)=Au.*C(aquiferIter).*exp(Au.*xDa); %Pressure derivative
            %ff(aquiferIter)=C(aquiferIter).*exp(Au.*xDa); %Pressure PD4 
            end
                end
            end
    f(omegaIter,:)=ff;
        end

% gavstehmatrix calculates the inverse Laplace transform according to
% the Gaver Stehfest method
% funname - the function to evaluated
% t - the current timestep (transform argument)
% L - some sort of precision
% matlength - the length of the omega vector
% matwidth - the number of aquifers + 2
% ilt - the inverse of the transform L^-1 matrix for different values of
% omega and different aquifers (matlength x matwidth)

function ilt=gavstehmatrix(funname,t,L,matlength,matwidth)
    nn2 = L/2;
    nn21= nn2+1;
    for n = 1:L
        z = 0.0;
        for k = floor( ( n + 1 ) / 2 ):min(n,nn2) %argument for sum in Stehfest V_i
                z = z + ((k^nn2)*factorial(2*k))/ ... %sum for Stehfest V_i
                    (factorial(nn2-k)*factorial(k)*factorial(k-1)* ...
                    factorial(n-k)*factorial(2*k - n));
        end
        v(n)=(-1)^(n+nn2)*z; %Stehfest V_i
    end
    sum = zeros(matlength,matwidth);
    ln2_on_t = log(2.0) / t;
    for n = 1:L
        p = n * ln2_on_t;
        sum = sum + v(n) .* feval(funname,p); %feval(fname,x1): evaluate function fname for x1
    end
    ilt = sum .* ln2_on_t;   %simple Stehfest version / fN(t) or (3) on handwritten Gaver Stehfest  