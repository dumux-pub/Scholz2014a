function Faultleakage

% By: Mehdi Zeidouni
% Zeidouni, M., (2012), Analytical model of leakage through fault to overlying formations, Water Resources Research, 48, W00N02.
% January 14, 2014
%Edited by: Simon Scholz 


clc
clear all
global N au TD etaD

%User input
q=0.04349;    %injection rate (m3/s)
aa=13500;     %well-fault distance (m)
eta=1.22;     %Diffusivity coefficient of the injection zone (m2/s)
k=1.1e-13;    %Injection zone permeability (m2)
h=20;         %Injection zone thickness (m)
mu=1e-3;      %fluid viscosity (Pa.s)

N=4; %Number of overlying aquifers
etau=[0.005556; 0.3147; 2.22; 1.481];       %Upper aquifers diffusivities, [injection to GOK] (m2/s)
ku=[1e-16; 1e-14; 1e-13; 1e-13];            %Upper aquifers permeabilities, [injection to GOK] (m2)
hu=[20; 900; 350; 400];                     %Upper aquifers thicknesses, [injection to GOK] (m)
kf=1e-12*ones(N,1);     %Upfault permeability (m2)
wf=100*ones(N,1);       %fault width (m)
hcaprock=[0;50;0;80];   %Cap-rock thickness (m) 
%time=1:20:50; %logspace(0,5,50); %creation of vector with logarithmic timesteps
tm=(86400*365)*[0.01 0.25 0.5 1 1.5 2 3 4 5 6 7 8 9 10 12 15 18 20 25 30 40 50];    %times at which leakage rate to be evaluated (s)

%definitions
tDm=eta*tm/aa^2; %calculate dimensionless time 
for aquiferIter=1:N
    if aquiferIter==1
        L(aquiferIter)=(h+hu(aquiferIter))/2+hcaprock(aquiferIter); %length of flow path for injection zone
    else
        L(aquiferIter)=(hu(aquiferIter)+hu(aquiferIter-1))/2+hcaprock(aquiferIter); %length of flow path for upper aquifers
    end
end

%Dimensionless terms from Paper (Page 6 Eq. 22)
au=aa*kf.*wf./(2*k*h*L');    %calculate alpha (measure for aquifer lateral transmissibility) for upper aquifer
TD=(ku.*hu)./(k.*h);    %dimensionless transmissivity TD=(ku.*hu)./(k.*h) 
                        %set TD=1e9 for Dirichlet boundary condition. Here TD(4)=1e9
etaD=etau/eta;          %dimensionless diffusivity

qlD=RateSim(tDm);

%figure;plot(tDm,qlD) %Dimensionless time used here, for time use 'tm/86400/365'
%xlabel('tDm')
%ylabel('q_l_D')
assignin('base','qlD', qlD);
assignin('base','tm', tm);
figure;plot(tm/86400/365,qlD)
title('multi 3 confining bottom top')
xlabel('Time [years]')
ylabel('leakage rate q_l / injection rate q_i = dimensionless leakage q_l_D [-]')

    function qlD=RateSim(tDm)
        
        global xD w N TD
        xD=0;
        Fs = 1.519; %or use 1044; %inverse sampling frequency
        NN = 100; %or use 1024*8*8; %number of sample points for y
        k=[0:NN/2-1 -NN/2:-1]; % N needs to be even
        w = k/Fs; % omega vector, max(w) should be a range beyond which qD is zero
        
        % dimensionless flux vector q/q_injection
        qlD=zeros(N,length(tDm));
        
        for timeIter=1:length(tDm) % loop over time steps
            % Calculate L^-1(F(p)), i.e the inverse Laplace transform to
            % obtain the Fourier transformed pressure
            data_w=gavstehmatrix('qD',tDm(timeIter),16,length(w),N+2); % Fourier transformed pressures at xd = 0, i.e. fault zone
            % Calculate F^-1(p) to obtain the pressure in the space time
            % domain. Divide by 2pi, see paragraph 52 in paper.
            data_f = (1/(2*pi))*fft(data_w);
            % Divide the pressure by the inverse sampling frequency
            data_f = (data_f)./Fs;
            % Shift the data, see paragraph 53 in paper.
            for aquiferIter=1:(N+2)
                data_f(:,aquiferIter)=abs(fftshift(data_f(:,aquiferIter)));
            end
            
            for aquiferIter=N+2:-1:3 % loop over aquifers
                for sampleIter=2:NN % loop over y sampling points
                    % see Equation 26 in paper
%                       qlD(aquiferIter-2,timeIter)=qlD(aquiferIter-2,timeIter) ...
%                       +2*au(aquiferIter-2)*((data_f(sampleIter,aquiferIter-1)-data_f(sampleIter,aquiferIter) ...
%                       +data_f(sampleIter-1,aquiferIter-1)- ...
%                       data_f(sampleIter,aquiferIter)))*(2*pi*Fs/NN)/2;
                  % see Equation 27 in paper
                    qlD(aquiferIter-2,timeIter)=qlD(aquiferIter-2,timeIter)+ ...
                        2*TD(aquiferIter-2)*(data_f(sampleIter,aquiferIter)+ ...
                        data_f(sampleIter-1,aquiferIter))*(2*pi*Fs/NN)/2;
                    %(2*pi*Fs/NN) = dyD i.e dimensionless incremantion in y
                    %direction
                end
            end
        end
   
% qD calculates the transformed pressures in the Laplace-Fourier domain
% at the fault zone in each aquifer.
% s - Laplace parameter s
% f - vector of transformed pressures at the fault zone for different
% values of omega (vector w)

    function f=qD(s)
        global xD au TD etaD w N
        f=zeros(length(w),N+2);
        
        for omegaIter=1:length(w)
            A= sqrt(w(omegaIter)^2+s);
            for aquiferIter=1:N
                Au(aquiferIter)=sqrt(w(omegaIter)^2+s/etaD(aquiferIter));
            end
            
            C0=exp(-A)/(2*s*A);
            H=zeros(N+2,N+2);
            
            H(1,1)=-1;
            H(1,2)=1;
            H(2,1)=A;
            H(2,2)=A+2*au(1);
            H(2,3)=-2*au(1);
            
            for aquiferIter=3:(N+2)
                for ll=(aquiferIter-1):(N+2)
                    if ll==(aquiferIter-1)
                        H(aquiferIter,ll)=-au(ll-1);
                    elseif ll==aquiferIter
                        H(aquiferIter,ll)=au(ll-2)+TD(ll-2)*Au(ll-2);
                    else
                        H(aquiferIter,ll)=TD(ll-2)*Au(ll-2);
                    end
                end
            end
            
            F=zeros(N+2,1);
            F(1)=C0;
            F(2)=C0*A;
            
            C=inv(H)*F;
            
            %Calculate the pressures in the Fourier-Laplace domain at xD=0,
            %i.e. the fault zone for every aquifer for given omega
            ff=zeros(1,N+2);
            for aquiferIter=1:(N+2) %iterate over aquifers + 2
                if aquiferIter==1
                    ff(aquiferIter)=(1./(2*s*A)).*exp(-A)+C(1); %Pressure
                elseif aquiferIter==2
                    ff(aquiferIter)=C(2); %Pressure
                else
                    ff(aquiferIter)=C(aquiferIter).*Au(aquiferIter-2); %Pressure derivative with respect to xd for xD = 0
                end
            end
            f(omegaIter,:)=ff;
        end
   
% gavstehmatrix calculates the inverse the Laplace transform according to
% the Gaver Stehfest method
% funname - the function to evaluated
% t - the current timestep (transform argument)
% L - some sort of precision
% matlength - the length of the omega vector
% matwidth - the number of aquifers + 2
% ilt - the inverse of the transform L^-1 matrix for different values of
% omega and different aquifers (matlength x matwidth)

    function ilt=gavstehmatrix(funname,t,L,matlength,matwidth)
        nn2 = L/2;
        nn21= nn2+1;
        
        for n = 1:L
            z = 0.0;
            for k = floor( ( n + 1 ) / 2 ):min(n,nn2) %argument for sum in Stehfest V_i
                z = z + ((k^nn2)*factorial(2*k))/ ...  %sum for Stehfest V_i
                    (factorial(nn2-k)*factorial(k)*factorial(k-1)* ...
                    factorial(n-k)*factorial(2*k - n));
            end
            v(n)=(-1)^(n+nn2)*z;  %Stehfest V_i
        end
        
        sum = zeros(matlength,matwidth);
        ln2_on_t = log(2.0) / t;
        for n = 1:L
            p = n * ln2_on_t;
            sum = sum + v(n) .* feval(funname,p);  %feval(fname,x1): evaluate function fname for x1
        end
        ilt = sum .* ln2_on_t; %simple Stehfest version / fN(t) or (3) on handwritten Gaver Stehfest