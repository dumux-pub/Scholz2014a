function Faultleakage

% By: Mehdi Zeidouni
% Zeidouni, M., (2012), Analytical model of leakage through fault to overlying formations, Water Resources Research, 48, W00N02. 
% January 14, 2014
%Edited by: Simon Scholz


clc
clear all
global N au TD etaD w s xD Fs kw NN

%User input
q=0.005;    %injection rate (m3/s)
aa=500;     %well-fault distance (m)
eta=10;     %Diffusivity coefficient of the injection zone (m2/s)
k=1e-13;    %Injection zone permeability (m2)
h=60;       %Injection zone thickness (m)
mu=1e-3;    %fluid viscosity (Pa.s)

N=15; 
etau=10*ones(N,1);      %Upper aquifers diffusivities (m2/s)
ku=1e-13*ones(N,1);     %Upper aquifers permeabilities (m2)
hu=40*ones(N,1);        %Upper aquifers thicknesses (m)
kf=1e-14*ones(N,1); %Upfault permeability (m2)
wf=1.68*ones(N,1);      %fault width (m)
hcaprock=30*ones(N,1);  %Cap-rock thickness (m) 
time=logspace(-3,2,30); %creation vector with logarithmic timesteps
tm=(86400*365)*time;    %times at which leakage rate to be evaluated (s)

%definitions
tDm=eta*tm/aa^2; %calculate dimensionless time
for aquiferIter=1:N     
    if aquiferIter==1
        L(aquiferIter)=(h+hu(aquiferIter))/2+hcaprock(aquiferIter);
    else
        L(aquiferIter)=(hu(aquiferIter)+hu(aquiferIter-1))/2+hcaprock(aquiferIter);
    end
end

%Dimensionless terms from Paper (Page 6 Eq. 22)
au=aa*kf.*wf./(2*k*h*L');   %calculate alpha (measure for aquifer lateral transmissibility) for upper aquifer
TD=(ku.*hu)./(k.*h);        %dimensionless transmissivity TD=(ku.*hu)./(k.*h);
etaD=etau/eta;              %dimensionless diffusivity

%Definitions for qlD=RateSim(tDm)
xD=0;
Fs = 1044;          % inverse sampling frequency
NN = 1024*8*8;      % number of sample points for y
dyD=(2*pi*Fs/NN);   %dimensionless increment in y 
kw=[0:NN/2-1 -NN/2:-1]; % N needs to be even
w = kw/Fs;              % w vector, max(w) should be a range beyond which qD is zero


qlD=RateSim(tDm);

%Output for Matlab Workspace
assignin('base', 'N', N);
assignin('base', 'qlD', qlD);
assignin('base', 'tDm', tDm);
assignin('base', 'tm', tm);
assignin('base', 'TD', TD);
assignin('base', 'etaD', etaD);
assignin('base', 'au', au);
assignin('base', 'w', w);
assignin('base', 'L', L);
assignin('base', 's', s);

% Plot leakage
figure;semilogx(tm,qlD)
xlabel('Time tm multi')
ylabel('q_l_D')


function qlD=RateSim(tDm)

global xD w au N TD Fs NN

qlD=zeros(N,length(tDm));

for timeIter=1:length(tDm)  % loop over time steps
    % Calculate L^-1(F(p)), i.e the inverse Laplace transform to
    % obtain the Fourier transformed pressure
    data_w=(1/(2*pi))*gavstehmatrix('qD',tDm(timeIter),16,length(w),N+2);   % Fourier transformed pressures at xd = 0, i.e. fault zone
    % Calculate F^-1(p) to obtain the pressure in the space time
    % domain. Divide by 2pi, see paragraph 52 in paper.
    data_f = fft(data_w);   % Shift the data, see paragraph 53 in paper.
    data_f = (data_f)./Fs; % Divide the pressure by the inverse sampling frequency
    for aquiferIter=1:(N+2)
        data_f(:,aquiferIter)=abs(fftshift(data_f(:,aquiferIter)));
    end
    for aquiferIter=N+2:-1:3 % loop over aquifers
        for sampleIter=2:NN % loop over y sampling points
                    % see Equation 26 in paper
%                       qlD(aquiferIter-2,timeIter)=qlD(aquiferIter-2,timeIter) ...
%                       +2*au(aquiferIter-2)*((data_f(sampleIter,aquiferIter-1)-data_f(sampleIter,aquiferIter) ...
%                       +data_f(sampleIter-1,aquiferIter-1)- ...
%                       data_f(sampleIter,aquiferIter)))*(2*pi*Fs/NN)/2;
                  % see Equation 27 in paper
                  qlD(aquiferIter-2,timeIter)=qlD(aquiferIter-2,timeIter)+...
                      2*TD(aquiferIter-2)*(data_f(sampleIter,aquiferIter)+...
                      data_f(sampleIter-1,aquiferIter))*(2*pi*Fs/NN)/2;
            %(2*pi*Fs/NN) = dyD i.e dimensionless incremantion in y %direction
        end
    end
end

% qD calculates the transformed pressures in the Laplace-Fourier domain
% at the fault zone in each aquifer.
% s - Laplace parameter s
% f - vector of transformed pressures at the fault zone for different
% values of omega (vector w)

function f=qD(s)
global xD au TD etaD w N Au
f=zeros(length(w),N+2);

for omegaIter=1:length(w)
    A= sqrt(w(omegaIter)^2+s);
    for aquiferIter=1:N
        Au(aquiferIter)=sqrt(w(omegaIter)^2+s/etaD(aquiferIter));
    end
    
    C0=exp(-A)/(2*s*A);
    H=zeros(N+2,N+2);
    
    H(1,1)=-1;
    H(1,2)=1;
    H(1,3)=0; %new
    H(2,1)=A;
    H(2,2)=A+2*au(1); %edited
    H(2,3)=-2*au(1); %edited
    
    
    for aquiferIter=3:(N+2)
        for ll=(aquiferIter-1):(N+2)
            if ll==(aquiferIter-1)
                H(aquiferIter,ll)=-au(ll-1);
            elseif ll==aquiferIter
                H(aquiferIter,ll)=au(ll-2)+TD(ll-2)*Au(ll-2);
            else
                H(aquiferIter,ll)=TD(ll-2)*Au(ll-2);
            end
        end
     end
    
    F=zeros(N+2,1);
    F(1)=C0;
    F(2)=C0*A;
    F(3)=0;
    
    C=inv(H)*F; %Zeidouni original
    %C=H\F; %Scholz test
    
    %Calculate the pressures in the Fourier-Laplace domain at xD=0,
    %i.e. the fault zone for every aquifer for given omega
    ff=zeros(1,N+2);
     for aquiferIter=1:(N+2) %iterate over aquifers
        if aquiferIter==1
            ff(aquiferIter)=(1./(2*s*A)).*exp(-A)+C(1); %Pressure
        elseif aquiferIter==2
            ff(aquiferIter)=C(2); %Pressure
        else
            ff(aquiferIter)=C(aquiferIter).*Au(aquiferIter-2); %Pressure derivative with respect to xd for xD=0
       end
    end
    f(omegaIter,:)=ff;
end

% gavstehmatrix calculates the inverse the Laplace transform according to
% the Gaver Stehfest method
% funname - the function to evaluated
% t - the current timestep (transform argument)
% L - some sort of precision
% matlength - the length of the omega vector
% matwidth - the number of aquifers + 2
% ilt - the inverse of the transform L^-1 matrix for different values of
% omega and different aquifers (matlength x matwidth)

function ilt=gavstehmatrix(funname,t,L,matlength,matwidth)
nn2 = L/2;
nn21= nn2+1;

for n = 1:L
    z = 0.0;
	for k = floor( ( n + 1 ) / 2 ):min(n,nn2) %argument for sum in Stehfest V_i
        z = z + ((k^nn2)*factorial(2*k))/ ...  %sum for Stehfest V_i
            (factorial(nn2-k)*factorial(k)*factorial(k-1)* ...
            factorial(n-k)*factorial(2*k - n));
    end
    v(n)=(-1)^(n+nn2)*z; %Stehfest V_i
end

sum = zeros(matlength,matwidth);
ln2_on_t = log(2.0) / t;
for n = 1:L
    p = n * ln2_on_t;
    sum = sum + v(n) .* feval(funname,p); %feval(fname,x1): evaluate function fname for x1
end 
	ilt = sum .* ln2_on_t; %simple Stehfest version / fN(t) or (3) on handwritten Gaver Stehfest