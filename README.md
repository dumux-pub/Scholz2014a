## Summary

This folder contains the analytical and numerical model results presented in the Bachelor Thesis of Simon Scholz. The folder structure is as follows:

folder "analytical_results":
- contains all Matlab-scripts, figures and the pdf of the Bachelor-Thesis. For more information have a look at the README in "analytical_results".

folder "numerical_results":
- contains the log files of the two- and multilayer numerical simulations shown in the Bachelor-Thesis along with the Matlab-scripts to create plots from them. Check the READMEs within these folders for information on usage.
- Additionally the scripts for creating the two- and multilayer grids are included, check the README in "grids".

folder "dumux-scholz":
- contains the dune-module for making the numerical simulations. For dependencies check the README in "dumux-scholz".

## Installation
The easiest way to install this module is to create a new folder and to execute the file
[installScholz2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2014a/raw/2fc95f5068cf205d2568301fac375a560c0e6b56/Scholz2014a/installScholz2014a.sh)
in this folder.
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.
