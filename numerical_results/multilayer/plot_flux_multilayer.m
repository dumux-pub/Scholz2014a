%Plot the flux through the fault zone into the top aquifer for the generic
%multi-layer model

clear all;
plotName = 'multilayer';
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;

%initalization time and injection rate, check from dumux input file or end
%of log-file
tinit = 1e6;
injectionRate = 11.7259;
%array of logfiles to read
logFileName{1} = 'multilayer.log';
%store the data from the logfile in outmat.mat
outputMatName = 'outmat.mat';
%the list of strings to be searched in the log file
strList{timeIdx} = 'Time:';
strList{fluxFaultIdx} = 'Flux Across Fault (holes in Rupel and saltwall): Total Flux:';
strList{fluxRupelIdx} = 'Flux Across Rupel: Total Flux:';
strList{fluxHolesIdx} = 'Flux Across Holes Rupel: Total Flux:';
strList{fluxTotalIdx} = 'Flux into Ter Quar: Total Flux:';


%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    outputReader(strList, logFileName{logIter}, outputMatName);
    load(outputMatName);
    tInitIdx = find((output(:, timeIdx) - tinit) == 0);
    time{logIter} = (output(tInitIdx:length(output), timeIdx) - tinit)/365/24/3600;
    fluxTotal{logIter} = (output(tInitIdx:length(output), fluxTotalIdx) - output(tInitIdx, fluxTotalIdx))*(-1)/injectionRate;
    fluxFault{logIter} = (output(tInitIdx:length(output), fluxFaultIdx) - output(tInitIdx, fluxFaultIdx))*(-1)/injectionRate;
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 12;
numberFontSize = 12;
strFontUnit = 'pixels';
iResolution = 150;
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');
%presplot = plot(time, dpM1, time, dpM2);

for logIter = 1:length(logFileName)
    fluxplot(logIter) = plot(time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot));
    set(fluxplot(logIter), 'LineWidth',2);
    hold on;
end
set(fluxplot(1), 'color', [0 0 1]);
title('Flux into shallow aquifers through salt flank');
ylabel('Mass Flow Rate / Injection Rate [-]');
xlabel('Time [years]');
axis([0,100,0,1])
axis square;

set(Fig,'position',[0 0, 12, 12])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc') 

