%Plot the flux through the fault zone into the top aquifer for the generic
%two-layer model

clear all;
plotName = 'twolayer';
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;
presM2Idx = 6;

%initalization time and injection rate, check from dumux input file or end
%of log-file
tinit = 1e6;
injectionRate = 21.745;
%array of logfiles to read
logFileName{1} = 'twolayer_neumann_50km.log';
logFileName{2} = 'twolayer_dirichlet_50km.log';
%logFileName{3} = 'twolayer_neumann_25km.log';
%logFileName{4} = 'twolayer_dirichlet_25km.log';
%logFileName{5} = 'twolayer_neumann_100km.log';
%logFileName{6} = 'twolayer_dirichlet_100km.log';

%store the data from the logfile in outmat.mat
outputMatName = 'outmat.mat';
%the list of strings to be searched in the log file
strList{timeIdx} = 'Time:';
strList{fluxFaultIdx} = 'Flux Across Fault (holes in Rupel and saltwall): Total Flux:';
strList{fluxRupelIdx} = 'Flux Across Rupel: Total Flux:';
strList{fluxHolesIdx} = 'Flux Across Holes Rupel: Total Flux:';
strList{fluxTotalIdx} = 'Flux into Ter Quar: Total Flux:';



%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    outputReader(strList, logFileName{logIter}, outputMatName);
    load(outputMatName);
    tInitIdx = find((output(:, timeIdx) - tinit) == 0);
    time{logIter} = (output(tInitIdx:length(output), timeIdx) - tinit)/365/24/3600;
    fluxTotal{logIter} = (output(tInitIdx:length(output), fluxTotalIdx) - output(tInitIdx, fluxTotalIdx))*(-1)/injectionRate;
    fluxFault{logIter} = (output(tInitIdx:length(output), fluxFaultIdx) - output(tInitIdx, fluxFaultIdx))*(-1)/injectionRate;
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 12;
numberFontSize = 12;
strFontUnit = 'pixels';
iResolution = 150;
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');
%presplot = plot(time, dpM1, time, dpM2);

for logIter = 1:length(logFileName)
    fluxplot(logIter) = plot(time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot));
    set(fluxplot(logIter), 'LineWidth',2);
    hold on;
end
set(fluxplot(1), 'color', [0 0 1]);
set(fluxplot(2), 'color', [0 0 1], 'LineStyle', '--');
% set(fluxplot(3), 'color', [0 0.5 0]);
% set(fluxplot(4), 'color', [0 0.5 0], 'LineStyle', '--');
% set(fluxplot(5), 'color', [1 0 0]);
% set(fluxplot(6), 'color', [1 0 0],'LineStyle',  '--');
title('Flux into shallow aquifers over salt flank');
ylabel('Mass Flow Rate / Injection Rate [-]');
xlabel('Time [years]');
%h_leg = legend('Neumann 25 km', 'Dirichlet 25 km', 'Neumann 50 km', 'Dirichlet 50 km', ...
%    'Neumann 100 km', 'Dirichlet 100 km', 'Location', 'NorthEast');
h_leg = legend('Neumann 50 km', 'Dirichlet 50 km', 'Location', 'NorthEast');
set(h_leg,'Interpreter','latex')
axis([0,100,0,1])
axis square;

set(Fig,'position',[0 0, 12, 12])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc') 


