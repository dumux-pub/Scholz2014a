% generic solling 3d grid
clear;

pathdgf = 'genericsolling_50km.dgf';
pathvtk = 'genericsolling_50km.vtk';

% object distance
distanceInjection = 15000;
saltDomeWidth = 1000;
Y = 5e4; % Domain length in y direction
X = Y+distanceInjection; % Domain length in x direction

Z = 3780; % Domain length in z direction
nY = 20; %approximate no of elements in Y direction

% cell sizes before injection in x
minCellSizeX_bi = 50;
maxCellSizeX_bi = 300;
% cell sizes after injection in x
minCellSizeX_ai = maxCellSizeX_bi;
maxCellSizeX_ai = 1e3;
nx_ai = 20;

% max/min cell sizes
minCellSizeY = 300;
maxCellSizeY = 1e3;



%bottom height of geological layers
quar = 300;
nQuar = 4;
ter = 700;
nTer = 5;
rc = 780;
nRc = 1;
oli = 1130;
nOli = 4;
kreide = 2030;
nKreide = 10;
obs = 2080;
nObs = 2;
mbs =   2100;
nMbs = 1;
sol = 2120;
nSol = 2;
umbs = 2230;
nUmbs = 4;
ubs = 2580;
nUbs = 3;
zec = Z;
nZec = 10;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%X-Vector
%dxLin_bs = 0:200:(saltDomeWidth-200);
%dxLog_bi = logSpacer(nx_bi, ...
%    distanceInjection, minCellSizeX_bi, maxCellSizeX_bi, saltDomeWidth);
%test = saltDomeWidth + minCellSizeX_bi + maxCellSizeX_bi:maxCellSizeX_bi:distanceInjection;
dxNeg = sort(-minCellSizeX_bi:-maxCellSizeX_bi:-(Y-distanceInjection));
dxLin = [dxNeg, 0:200:saltDomeWidth, (saltDomeWidth + minCellSizeX_bi):maxCellSizeX_bi:X];
%dxLog_ai= logSpacer(nx_ai, ...
%    X, minCellSizeX_ai, maxCellSizeX_ai, distanceInjection + maxCellSizeX_bi);
%dx = [dxLin_bs, dxLin_bi, dxLog_ai];
%dx = [dxLin_bs, dxLin_bi];
dx = dxLin;

%Y-Vector
dyLin = 0:minCellSizeY:(Y - minCellSizeY);
%dyLog = logSpacer(nY, Y, minCellSizeY, maxCellSizeY, distanceInjection/2);
%dy = [dyLin, dyLog];
dy = dyLin;

%Z-Vector
dzQuar = 0:quar/nQuar:quar;
dzTer = quar+(ter-quar)/nTer:(ter-quar)/nTer:ter;
dzRc = ter+(rc-ter)/nRc:(rc-ter)/nRc:rc;
dzOli = rc+(oli-rc)/nOli:(oli-rc)/nOli:oli;
dzKreide = oli+(kreide-oli)/nKreide:(kreide-oli)/nKreide:kreide;
dzObs = kreide+(obs-kreide)/nObs:(obs-kreide)/nObs:obs;
dzMbs = obs+(mbs-obs)/nMbs:(mbs-obs)/nMbs:mbs;
dzSol = mbs+(sol-mbs)/nSol:(sol-mbs)/nSol:sol;
dzUmbs = sol+(umbs-sol)/nUmbs:(umbs-sol)/nUmbs:umbs;
dzUbs = umbs+(ubs-umbs)/nUbs:(ubs-umbs)/nUbs:ubs;
%dzZec = ubs+(zec-ubs)/nZec:(zec-ubs)/nZec:zec;
dz = [dzQuar, dzTer, dzRc, dzOli, dzKreide, dzObs, dzMbs, dzSol, dzUmbs, dzUbs]; 

noNodes = length(dx)*length(dy)*length(dz);
display(noNodes);
%Create the dgf file
dgfWriter(pathdgf, dx, dy, dz);

%Create the vtk file
vtkWriter(pathvtk, dx, dy, dz);

