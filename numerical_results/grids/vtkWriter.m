%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vtk writer for 2 or 3d rectilinear grids
% pathToVTK - path for vtk file
% created by Alex Kissinger
% x - vector of x coordinates
% y - vector of y coordinates
% z - vector of z coordinates
% coordMatrix - (optional) coordinate matrix containing a list of nodes for
% creating deformed rectlinear grids or 2d surface grids in 3d space.
% Dimensions: coordMatrix(nodes, 2d or 3d coordinates) or 
% coordMatrix(2d or 3d coordinates, nodes)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function vtkWriter(pathToVTK, x, y, z, coordMatrix)

if(checkUnique(x) ~= 1 || checkUnique(y) ~= 1)
    error('Input vectors are not unique');
end
if(checkAscending(x) ~= 1 || checkAscending(y) ~= 1)
    error('Input vectors are not ascending');
end

if (length(z) > 1)
    if (checkUnique(z) ~= 1)
        error('Input vectors are not unique');
    end
    if (checkAscending(z) ~= 1)
        error('Input vectors are not ascending');
    end
end

if(nargin == 4 || nargin == 5) %check whether number of input parameters
    %is correct
    fid = fopen(pathToVTK,'w');
    fprintf(fid, '# vtk DataFile Version 2.0\n');
    fprintf(fid, 'Rectilinear Grid\n');
    fprintf(fid, 'ASCII\n');
    fprintf(fid, 'DATASET UNSTRUCTURED_GRID\n');
    fprintf(fid, 'POINTS %d FLOAT\n', length(x)*length(y)*length(z));
    counter = 1;
    nodeNo(1)=0;
    if(nargin == 4) %write the nodes for the not deformed case
        if(length(z) > 1) %3d case
            for l=1:length(z)
                for j=1:length(y)
                    for i=1:length(x)
                        nodeNo(counter) = counter-1;
                        counter = counter+1;
                        dx = x(i);
                        dy = y(j);
                        dz = z(l);
                        fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,dz);
                    end
                end
            end
        else  %2d case
            for j=1:length(y)
                for i=1:length(x)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    dx = x(i);
                    dy = y(j);
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,0.0);
                end
            end
        end
        
    else %write the nodes for the deformed case
        dim = size(coordMatrix);
        if(min(size(coordMatrix)) == 3) %Nodes hat coordinates in three dimensions
            for i=1:max(size(coordMatrix))
                nodeNo(counter) = counter-1;
                counter = counter+1;
                if(dim(1) > dim(2)) % Check for the greater dimension of the matrix which should be nodes
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coordMatrix(i,:));
                else
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coordMatrix(:,i));
                end
            end
        elseif(min(size(coordMatrix)) == 2) %Nodes hat coordinates in two dimensions
            for i=1:max(size(coordMatrix))
                nodeNo(counter) = counter-1;
                counter = counter+1;
                if(dim(1) > dim(2)) % Check for the greater dimension of the matrix which should be nodes
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coordMatrix(i,:),0.0);
                else
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coordMatrix(:,i),0.0);
                end
            end
        else
            error('Invalid dimensions of coordinate matrix.');
        end
    end
    if(length(z) > 1) %3d case
        numCells = (length(x)-1)*(length(y)-1)*(length(z)-1);
        numNodePerCell = 8;
    else %2d case
        numCells = (length(x)-1)*(length(y)-1);
        numNodePerCell = 4;
    end

    
    fprintf(fid, 'CELLS %d %d\n', numCells, numCells*(numNodePerCell+1));
    
    counter = 0;
    if(length(z) > 1) %3d case
        for l=1:length(z) -1
            for j=1:length(y) -1
                for i=1:length(x) -1
                    counter = counter+1;
                    fprintf(fid, '%d %d %d %d %d %d %d %d %d\n', ...
                        numNodePerCell, ...
                        nodeNo(counter), ...
                        nodeNo(counter+1), ...
                        nodeNo(counter+length(x)), ...
                        nodeNo(counter+length(x)+1), ...
                        nodeNo(counter+length(x)*length(y)), ...
                        nodeNo(counter+length(x)*length(y)+1), ...
                        nodeNo(counter+length(x)*length(y)+length(x)), ...
                        nodeNo(counter+length(x)*length(y)+length(x)+1));
                end
                counter=counter+1;
            end
            counter=counter+length(x);
        end
    else %2d case
        counter = 0;
        for j=1:length(y) -1
            for i=1:length(x)-1
                counter = counter+1;
                fprintf(fid,  '%d %d %d %d %d\n', ...
                    numNodePerCell, ...
                    nodeNo(counter), ...
                    nodeNo(counter+1), ...
                    nodeNo(counter+length(x)), ...
                    nodeNo(counter+length(x)+1));
            end
            counter=counter+1;
        end
    end
    
    fprintf(fid, 'CELL_TYPES %d \n', numCells);
    
 
   if(length(z) > 1) %3d case
        cellType = 11; %VTK_VOXEL
   else %2d case
       cellType = 8; %VTK_PIXEL
   end
  if(length(z) > 1) %3d case
      for l=1:length(z) -1
          for j=1:length(y) -1
              for i=1:length(x) -1
                  fprintf(fid, '%d\n', cellType);
              end
          end
      end
  else %2d case
      for j=1:length(y) -1
          for i=1:length(x) -1
              fprintf(fid, '%d\n', cellType);
          end
      end
  end
else
    error('Wrong argument number for dgfWriterNormal function: %s\n', nargin);
end
end