% generic solling 3d grid
clear;

pathdgf = 'twolayerbenchmark_50km.dgf';
pathvtk = 'twolayerbenchmark_50km.vtk';
X = 55e3; % Domain length in x direction
Y = 50e3; % Domain length in y direction
Z = 150; % Domain length in z direction
nY = 20; %approximate no of elements in Y direction

faultWidth = 50;

%horizontal cell size
cellSize = 300;

% object distance
distanceInjectionFault = 5e3;

%bottom height of geological layers
quar = 50;
nQuar = 2;
rc = 100;
nRc = 2;
sol = 150;
nSol = 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%X-Vector injection is at x = 0
dx = [-distanceInjectionFault (-distanceInjectionFault + faultWidth) ...
    -cellSize:-cellSize:(-distanceInjectionFault + faultWidth +1e-6) 0:cellSize:X];
dx = sort(dx);

%Y-Vector
dy = [0 cellSize/2:cellSize:Y];


%Z-Vector
dzQuar = 0:quar/nQuar:quar;
dzRc = quar+(rc-quar)/nRc:(rc-quar)/nRc:rc;
dzSol = rc+(sol-rc)/nSol:(sol-rc)/nSol:sol;
dz = [dzQuar dzRc dzSol];

%Create the dgf file
dgfWriter(pathdgf, dx, dy, dz);

%Create the vtk file
vtkWriter(pathvtk, dx, dy, dz);

noNodes = length(dx)*length(dy)*length(dz);
display(noNodes);