###
# Creates a symbolic link to the specified file in the source directory
# ATTENTION: symbolic links are not supported by all file systems, e. g.
# it will not work on Windows.
#
# Arguments:
# - file_name:                    name of the file
###
macro(add_file_link file_name)
  # if present, add file link
  set(file ${CMAKE_CURRENT_SOURCE_DIR}/${file_name})
  if(EXISTS ${file})
    execute_process(
      COMMAND cmake -E create_symlink ${file} ${CMAKE_CURRENT_BINARY_DIR}/${file_name})
  endif()
endmacro()
