# DUNE/DUMUX VERSIONS
# dune-common
#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

# DUNE/DUMUX VERSIONS
# dune-alugrid
# master # 0ee2c3af9f8a132c0946ba3e1508a4a22d4af4de # 2016-04-29 17:04:26 +0200 # Robert K
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout releases/2.4-master-old
git reset --hard dd6058fe1e6992ee5a9d08dea3224c7b04c62dff 
cd ..

# dune-common
# releases/2.4 # fa6bc4af59352430cb05cdb488cbe3ae18dd9547 # 2016-05-28 06:44:34 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.4
git reset --hard fa6bc4af59352430cb05cdb488cbe3ae18dd9547
cd ..

# dune-geometry
# releases/2.4 # ac1fca4ff249ccdc7fb035fa069853d84b93fb73 # 2016-05-28 07:30:48 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.4
git reset --hard ac1fca4ff249ccdc7fb035fa069853d84b93fb73
cd ..

# dune-grid
# releases/2.4 # a2c9bf8c0f0fef025325e059a45b7744225df0d7 # 2016-05-28 07:35:13 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.4
git reset --hard a2c9bf8c0f0fef025325e059a45b7744225df0d7
cd ..

# dune-istl
# releases/2.4 # 8e13380949417233b1515c8610cb24950382c090 # 2016-05-28 07:31:43 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.4
git reset --hard 8e13380949417233b1515c8610cb24950382c090
cd ..

# dune-localfunctions
# releases/2.4 # b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a # 2016-05-28 07:35:51 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.4
git reset --hard b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a
cd ..

# dumux
# releases/2.7 # 80bd9b3c97d2fcd0191e014e81e01dfff37aaf48 # 2015-04-22 15:52:21 +0000 # Bernd Flemisch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/2.7
git reset --hard 80bd9b3c97d2fcd0191e014e81e01dfff37aaf48
cd ..

# the pub-module
# Scholz2014a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2014a.git

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=./Scholz2014a/Scholz2014a/Scholz2014a.opts all