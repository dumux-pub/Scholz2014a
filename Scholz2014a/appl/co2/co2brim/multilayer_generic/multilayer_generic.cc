/*****************************************************************************
 *   Copyright (C) 2011 by Bernd Flemisch                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief benchmark for multi-layer system with fault
 */

//define what kind of spatial params are to be used
#define SPATIALPARAMS 1 //1 = 1p, 2 = 2p, 3 = decoupled
//define whether the generic geometry is used or the complex
#define GEOMETRY 1 //0 = complex, 1 = generic multi layer, 2 = generic two layer

#include "config.h"

#include <appl/co2/co2brim/solling/problems/solling1p2cniproblem.hh>
//#include "problems/testproblem.hh"

//#include "test_problem.hh"

// #include <dune/grid/common/gridinfo.hh>
// 
#include <dumux/common/start.hh>
#include <dumux/common/propertysystem.hh>
// #include <dumux/common/parameters.hh>
// 
// #include <dune/common/exceptions.hh>
// #include <dune/common/mpihelper.hh>
// #include <dune/common/parametertreeparser.hh>
// 
// #include <iostream>
// #include <boost/format.hpp>
// 
// #include <fstream>
// #include <vector>
// #include <dune/grid/common/grid.hh>
// #include <dune/geometry/referenceelements.hh>
// #include <dune/grid/io/file/dgfparser/dgfwriter.hh>
////////////////////////
// the main function
////////////////////////


/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe List of Mandatory arguments for this program is:\n"
                                        "\t-tEnd                               The end of the simulation [s] \n"
                                        "\t-dtInitial                          The initial timestep size [s] \n"
                                        "\t-gridFile                           The file name of the file containing the grid \n"
                                        "\t                                        definition in DGF format\n"
                                        "\n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

//start for unitcube or other dgf file
int main(int argc, char** argv)
{
//    Dune::FMatrixPrecision<>::set_singular_limit(0);
//    typedef TTAG(OnePTwoCNIOutflowBoxProblem) ProblemTypeTag;
    typedef TTAG(Solling1p2cNIBoxProblem) ProblemTypeTag;
    return Dumux::start<ProblemTypeTag>(argc, argv, usage);
}










