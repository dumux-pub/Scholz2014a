// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of a virtual injection into the snohvit CO2 storage reservoir close
 * to a chimney structure (for dgf format).
 *
 *
 * This file contains the boundary and initial conditions of the snohvit-chimney scenario
 */
#ifndef DUMUX_SOLLING1P2CNI_PROBLEM_HH
#define DUMUX_SOLLING1P2CNI_PROBLEM_HH


#if HAVE_ALUGRID
#include <dune/grid/alugrid/2d/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#else
#warning ALUGrid is necessary for this test.
#endif

#include <dune/common/float_cmp.hh>
#include <dumux/implicit/1p2c/1p2cmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/box/intersectiontovertexbc.hh>
#include <appl/co2/snohvit/snohvit_co2tables.hh>
#include <dumux/material/components/co2.hh>
#include <dumux/material/components/co2tablereader.hh>
#include <dumux/material/components/brine_varSalinity.hh>
#include <dumux/material/components/h2o.hh>
#include "../volumevariables/solling1p2cvolumevariables.hh"


//the FVElemGeometry for calculating fluxes across layers
//#include "boxfluxcalculation/sollingboxfvelementgeometry.hh"

//#include "sollingspatialparams_1_layer.hh"
//#include "sollingspatialparams1p.hh"
//#include "../sollingspatialparams_alllayer.hh"
// The actual spatial params to be used

// The adapter spatial params to be used
//#include "../spatialparams/sollingspatialparams1p.hh"
//#include <appl/co2/snohvit/snohvit_co2tables.hh>
#include "../fluidsystems/watersaltfluidsystem.hh"
//#include <dumux/material/fluidsystems/h2on2liquidphasefluidsystem.hh>


#include <dumux/linear/amgbackend.hh>
//#include <dumux/linear/seqsolverbackend.hh>


//Define if non-isothermal
#define NONISOTHERMAL 0



//#include "../spatialparams/sollingspatialparams_5layer.hh"
//#include "../spatialparams/sollingspatialparams_alllayer.hh"
#if GEOMETRY == 1
#include "../spatialparams/sollingspatialparams_generic.hh"
#elif GEOMETRY == 2
#include "../spatialparams/sollingspatialparams_twolayer.hh"
#else
#include "../spatialparams/sollingspatialparams_complex.hh"
#endif
//#include "../spatialparams/testspatialparams.hh"
//#include "../spatialparams/sollingspatialparams1p.hh"
//#include <test/implicit/1p2cni/1p2coutflowspatialparams.hh>

namespace Dumux
{

template <class TypeTag>
class Solling1p2cNIProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties
{
NEW_PROP_TAG(CO2Table);
//SET_TYPE_PROP(SollingSpatialParams, ParentSpatialParams, Dumux::SollingSpatialParamsOneP);

#if NONISOTHERMAL
NEW_TYPE_TAG(Solling1p2cNIProblem, INHERITS_FROM(OnePTwoCNI, SollingSpatialParams));
NEW_TYPE_TAG(Solling1p2cNIBoxProblem, INHERITS_FROM(BoxModel, Solling1p2cNIProblem));
NEW_TYPE_TAG(Solling1p2cNICCProblem, INHERITS_FROM(CCModel, Solling1p2cNIProblem));
#else
NEW_TYPE_TAG(Solling1p2cNIProblem, INHERITS_FROM(OnePTwoC));
NEW_TYPE_TAG(Solling1p2cNIBoxProblem, INHERITS_FROM(BoxModel, Solling1p2cNIProblem));
NEW_TYPE_TAG(Solling1p2cNICCProblem, INHERITS_FROM(CCModel, Solling1p2cNIProblem));
#endif

//
//SET_TYPE_PROP(Solling1p2cNIBoxProblem, FVElementGeometry, Dumux::SollingBoxFVElementGeometry<TypeTag>);
// Set the grid type
//SET_TYPE_PROP(Solling1p2cNIProblem, Grid, Dune::CpGrid);
SET_TYPE_PROP(Solling1p2cNIProblem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);

// Set the problem property
SET_TYPE_PROP(Solling1p2cNIProblem, Problem, Dumux::Solling1p2cNIProblem<TypeTag>);

// Set the CO2 table to be used; in this case not the the default table
SET_TYPE_PROP(Solling1p2cNIProblem, CO2Table, Dumux::SnohvitCO2Tables::CO2Tables);

// set the fluid system
SET_PROP(Solling1p2cNIProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::FluidSystems::WaterSalt<Scalar> type;
};

// Set the spatial parameters
SET_TYPE_PROP(Solling1p2cNIProblem,
              SpatialParams,
              Dumux::SollingSpatialParams<TypeTag>);

// Set the volume variables property to allow for compressible solid phase
SET_TYPE_PROP(Solling1p2cNIProblem,
              VolumeVariables,
              Dumux::Solling1p2cVolumeVariables<TypeTag>);


//              Dumux::OnePTwoCOutflowSpatialParams<TypeTag>);

//SET_PROP(Solling1p2cNIProblem, FluidSystem)
//{ private:
//    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//public:
//    typedef Dumux::FluidSystems::H2ON2LiquidPhase<Scalar, false> type;
//};


// Set the spatial parameters
//SET_TYPE_PROP(Solling1p2cNIProblem,
//              SpatialParams,
//              Dumux::SollingSpatialParams<TypeTag>);

//! the CO2 Model and VolumeVariables properties
//SET_TYPE_PROP(Solling1p2cNIProblem, Model, SnohvitTwoPModel<TypeTag>);

//// Set the CO2 table to be used; in this case not the the default table
//SET_TYPE_PROP(Solling1p2cNIProblem, CO2Table, Dumux::SnohvitCO2Tables::CO2Tables);
// Set the salinity mass fraction of the brine in the reservoir
//SET_SCALAR_PROP(Solling1p2cNIProblem, ProblemSalinity, 1e-1);

// Enable partial reassembly of the jacobian matrix?
//SET_BOOL_PROP(Solling1p2cNIProblem, ImplicitEnablePartialReassemble, false);

// Enable reuse of jacobian matrices?
//SET_BOOL_PROP(Solling1p2cNIProblem, ImplicitEnableJacobianRecycling, false);

// Write the solutions of individual newton iterations?
//SET_BOOL_PROP(Solling1p2cNIProblem, NewtonWriteConvergence, false);

// Use forward differences instead of central differences
//SET_INT_PROP(Solling1p2cNIProblem, ImplicitNumericDifferenceMethod, +1);

// Linear solver settings
SET_TYPE_PROP(Solling1p2cNIProblem, LinearSolver, Dumux::AMGBackend<TypeTag> );
//SET_TYPE_PROP(Solling1p2cNIProblem, LinearSolver, Dumux::AMGBackend<TypeTag> );
//SET_TYPE_PROP(Solling1p2cNIProblem, LinearSolver, Dumux::ScaledSeqAMGBackend<TypeTag> );
//SET_TYPE_PROP(Solling1p2cNIProblem, LinearSolver, Dumux::BoxBiCGStabILU0Solver<TypeTag> );
//SET_TYPE_PROP(Solling1p2cNIProblem, LinearSolver, Dumux::ILUnBiCGSTABBackend<TypeTag> );
//SET_INT_PROP(Solling1p2cNIProblem, LinearSolverVerbosity, 0);
//SET_INT_PROP(Solling1p2cNIProblem, LinearSolverPreconditionerIterations, 1);
//SET_SCALAR_PROP(Solling1p2cNIProblem, LinearSolverPreconditionerRelaxation, 1.0);

// Enable gravity
SET_BOOL_PROP(Solling1p2cNIProblem, ProblemEnableGravity, true);
// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(Solling1p2cNIProblem, UseMoles, false);
// Define whether output should be ascii or binary format
SET_PROP(Solling1p2cNIProblem, VtkMultiWriter)
        {
        private:
            typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
        public:
            typedef typename Dumux::VtkMultiWriter<GridView, Dune::VTK::appendedraw> type;
        };


}

template <class TypeTag >
class Solling1p2cNIProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef Solling1p2cNIProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag,CO2Table) CO2Table;
    typedef Dumux::CO2<Scalar, CO2Table> CO2;
    typedef Dumux::H2O<Scalar> H2O;
    typedef Dumux::BrineVarSalinity<Scalar, H2O> Brine;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
//        saturationIdx = Indices::saturationIdx,
        pwIdx = Indices::pressureIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
//        SnIdx = Indices::snIdx,

        // equation indices
        contiWEqIdx = Indices::conti0EqIdx,
        transportEqIdx = Indices::transportEqIdx,
//        contiNEqIdx = Indices::contiNEqIdx,

        // phase indices
        wPhaseIdx = Indices::conti0EqIdx,
//        nPhaseIdx = Indices::nPhaseIdx,


        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP(TypeTag, ParameterTree) Params;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<1>::Iterator FaceIterator;
//    typedef typename GridView::template Codim<1>::Iterator IntersectionIterator;
    typedef typename GridView::template Codim<dim-1>::Iterator EdgeIterator;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::Intersection Intersection;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
//    typedef Dumux::SollingBoxFVElementGeometry<TypeTag>  SollingBoxFVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GridView::Grid::ctype CoordScalar;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;


    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dimWorld,dimWorld> FieldMatrix;
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };
    enum { useMoles = GET_PROP_VALUE(TypeTag, UseMoles) };
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases)};

public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     * \param lensLowerLeft Global position of the lenses lower left corner
     * \param lensUpperRight Global position of the lenses upper right corner
     */
    Solling1p2cNIProblem(TimeManager &timeManager,
                const GridView &gridView)
        : ParentType(timeManager, gridView),
      intersectionToVertexBC_(*this)
{
        // Gravity vector definition
        gravity_ = 0;
        gravity_[dim-1] = 9.81;

        Scalar maxMolalityNaCl;

        try
        {

            name_               = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);
            //Episode mangement
            initializationPeriod_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InitializationPeriod);
            tInjectionEnd_    = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TInjectionEnd);
            injectionEpisodeLength_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InjectionEpisodeLength);
            postInjectionEpisodeLength_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PostInjectionEpisodeLength);
            postInitializationTimeStepSize_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PostInitializationTimeStepSize);
            episodeOutput_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.EpisodeOutput);

            // Fluidsystem initialization
            nTemperature_       = GET_RUNTIME_PARAM(TypeTag, int, FluidSystem.NTemperature);
            nPressure_          = GET_RUNTIME_PARAM(TypeTag, int, FluidSystem.NPressure);
            pressureLow_        = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.PressureLow);
            pressureHigh_       = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.PressureHigh);
            temperatureLow_     = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.TemperatureLow);
            temperatureHigh_    = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.TemperatureHigh);

            // Injection coordinates
            injectionRate_      = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InjectionRate);
            xInjCoord_	      = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.XInjCoor);
            yInjCoord_	      = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.YInjCoor);
            deltaX_	      = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DeltaXY);
            zInjCoord_		= GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.ZInjCoord);
            deltaZ_		= GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DeltaZ);

            //Measurement Coordinates
            m1_[0]        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.M1x);
            m1_[1]        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.M1y);
            m1_[dimWorld-1]       = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.M1z);
            m2_[0]        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.M2x);
            m2_[1]        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.M2y);
            m2_[dimWorld-1]       = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.M2z);

            //boundary conditions
            topOfReservoir_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TopOfReservoir);
            pressureTOR_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PressureTOR);
            gWRegenerationRate_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.GWRegenerationRate);
            topAquiferMissing_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.TopAquiferMissing);
            closed_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.Closed);
            dirichletAtTop_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.DirichletAtTop);
            useCO2VolumeEquivalentInjRate_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.UseCO2VolumeEquivalentInjRate);

            //initial conidtions
            massReductionFactor_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MassReductionFactor);
            applyGeothermalGradient_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.ApplyGeothermalGradient);
            enableSaltTransport_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.EnableSaltTransport);
            depthSalt_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DepthSalt);
            saltGrad_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.SaltGrad);
            maxMolalityNaCl = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MaxMolalityNaCl);


        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the problem file!\n";
            exit(1);
        }


        eps_ = 1e-6;



        // initialize the tables of the fluid system
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                /*Tmax=*/temperatureHigh_,
                /*nT=*/nTemperature_,
                /*pmin=*/pressureLow_,
                /*pmax=*/pressureHigh_,
                /*np=*/nPressure_);

        //initialize the maximum salt mass fraction [kg Salt/kg Brine] out of the maximum salt molality [kg Salt/kg Water]
        Scalar massFrac = maxMolalityNaCl;
        Scalar massFracLastIter = 0;
        Scalar T = 293.15; // [K] -> 20°C
        Scalar p = 1e5; // [Pa] -> 1 bar
        Scalar densityW, densityB;

        while(std::abs(massFrac - massFracLastIter) > eps_)
        {
            massFracLastIter = massFrac;
            densityW = H2O::liquidDensity(T, p);
            densityB = Brine::liquidDensity(T, p, massFrac);
            massFrac = maxMolalityNaCl*densityW/densityB;
        }
        maxNaClMassFrac_ = massFrac;
        std::cout<<"Max Mass Fraction NaCl: "<<maxNaClMassFrac_<<std::endl;

        //initialize variables on all processes
        m1GlobalIdx_ = -1;
        m2GlobalIdx_ = -1;
        injGlobalIdx_.resize(1);
        injGlobalIdx_[0] = -1;



        //read permeability and porosity from grid file
        GridPointer *gridPtr = &GridCreator::gridPtr();
//        Model *modelPtr = &this->model();
        this->spatialParams().setParams(gridPtr, this);

        //Find global index of injection cells and box volumes
        int injNoNodes = 0;
//        int boundaryNodes = 0;
        injVolume_ = 0.;
        Scalar minElementVolume = 1e10;
        GlobalPosition minElementCenter(0);
        FVElementGeometry fvGeometry;
        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator elemEndIt = this->gridView().template end<0>();
        PrimaryVariables initialValues(0);
        Scalar injDensityCO2(1);
        Scalar injDensityBrine(1);

        for (; eIt != elemEndIt; ++eIt)
        {

            fvGeometry.update(this->gridView(), *eIt);
            GlobalPosition elementCenter = eIt->geometry().center();
            minElementVolume = std::min(minElementVolume, eIt->geometry().volume());
            minElementCenter = minElementVolume == eIt->geometry().volume() ? elementCenter : minElementCenter;
            //                int numVerts = eIt->template count<dim> ();

            //iterate over all degrees of freedom (i.e. nodes or element centers)
            for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
            {
                GlobalPosition globalPos(0);
                int dofIdxGlobal = 0;
                if(GET_PROP_VALUE(TypeTag, ImplicitIsBox))
                {
                    globalPos = eIt->geometry().corner(scvIdx);
                    dofIdxGlobal = this->vertexMapper().subIndex(*eIt, scvIdx, dofCodim);
                }

                else
                {
                    globalPos = eIt->geometry().center();
                    dofIdxGlobal = this->elementMapper().subIndex(*eIt, scvIdx, dofCodim);
                }

                //initialize the fluxCalculationElement_ vector
                fluxLayerBase_.resize(this->elementMapper().index(*eIt)+1);
                if(this->spatialParams().findFluxLineIdx(*eIt) == 1)
                    fluxLayerBase_[this->elementMapper().index(*eIt)] = 1;
                else
                    fluxLayerBase_[this->elementMapper().index(*eIt)] = -1e10;

                fluxLayer_.resize(this->elementMapper().index(*eIt)+1);
                if(this->spatialParams().findFluxLineIdx(*eIt) == 1)
                    fluxLayer_[this->elementMapper().index(*eIt)] = 1;
                else
                    fluxLayer_[this->elementMapper().index(*eIt)] = -1e10;

                //initialize the pressure map
                initial_(initialValues, globalPos);
                initialPressureMap_[dofIdxGlobal]= initialValues[pressureIdx];

                if(eIt->partitionType() == Dune::InteriorEntity)
                {

                    if(isInjectionCell_(globalPos))
                    {
                        injNoNodes += 1;
                        injGlobalIdx_.resize(injNoNodes);
                        injGlobalIdx_[injNoNodes-1]= dofIdxGlobal;
                        injVolume_ += fvGeometry.subContVol[scvIdx].volume;

                        std::cout<<"Global Position: "<<globalPos<<" Rank: "<<this->gridView().comm().rank()<<
                                " Layer Index: "<<this->spatialParams().findLayerIdx(*eIt)<<std::endl;
                        PrimaryVariables initialValues(0);
                        initial_(initialValues, globalPos);
                        std::cout<<"Pressure at Injection: "<< initialValues[pressureIdx]<<std::endl;
                        std::cout<<"Temperature at Injection: "<< temperatureAtPos(globalPos)<<std::endl;
                        injDensityCO2 = CO2::gasDensity(temperatureAtPos(globalPos), initialValues[pressureIdx]);
                        injDensityBrine = Brine::liquidDensity(temperatureAtPos(globalPos), initialValues[pressureIdx], 0.);
                    }


                    //Measurement point 1
                    if(globalPos[0] > m1_[0] - deltaX_ && globalPos[0] < m1_[0] + deltaX_
                            && globalPos[1] < m1_[1] + deltaX_ && globalPos[1] > m1_[1] - deltaX_
                            && globalPos[2] < m1_[dimWorld-1] + deltaZ_ && globalPos[2] > m1_[dimWorld-1] - deltaZ_)
                    {
                        std::cout<<"Found measurementpoint 1"<<" Rank: "<<this->gridView().comm().rank()<<std::endl;

                        m1GlobalIdx_ = dofIdxGlobal;
                    }

                    //Measurement point 2
                    if(globalPos[0] > m2_[0] - deltaX_ && globalPos[0] < m2_[0] + deltaX_
                            && globalPos[1] < m2_[1] + deltaX_ && globalPos[1] > m2_[1] - deltaX_
                            && globalPos[2] < m2_[dimWorld-1] + deltaZ_ && globalPos[2] > m2_[dimWorld-1] - deltaZ_)
                    {
                        std::cout<<"Found measurementpoint 2"<<" Rank: "<<this->gridView().comm().rank()<<std::endl;

                        m2GlobalIdx_ = dofIdxGlobal;

                    }


                }

            }
        }
        //adjust CO2 injection rate to brine
        if(useCO2VolumeEquivalentInjRate_)
            injectionRate_ *= injDensityBrine/injDensityCO2;


        std::cout<<"Volume equivalent brine injection rate = "<<injectionRate_<<std::endl;
        //iterate over all element faces
        FaceIterator faceIt = this->gridView().template begin<1>();
        FaceIterator faceEndIt = this->gridView().template end<1>();
        GlobalPosition minFaceElementCenter(0);
        Scalar minFaceArea = 1e10;
        for(; faceIt != faceEndIt; ++faceIt)
        {
            minFaceArea = std::min(minFaceArea, faceIt->geometry().volume());
//            minFaceElementCenter = minFaceArea == faceIt->geometry().volume() ? faceIt->geometry().center() : minFaceElementCenter;
        }

        //iterate over all element edges
        EdgeIterator edgeIt = this->gridView().template begin<dim-1>();
        EdgeIterator edgeEndIt = this->gridView().template end<dim-1>();
        GlobalPosition minEdgeElementCenter(0);
        Scalar minEdgeLength = 1e10;
        for(; edgeIt != edgeEndIt; ++edgeIt)
        {
            minEdgeLength = std::min(minEdgeLength, edgeIt->geometry().volume());
//            minEdgeElementCenter = minEdgeLength == edgeIt->geometry().volume() ? edgeIt->geometry().center() : minEdgeElementCenter;
        }

        if (this->gridView().comm().size() > 1)
        {
            injVolume_ = this->gridView().comm().sum(injVolume_);
            injNoNodes = this->gridView().comm().sum(injNoNodes);
            minFaceArea = this->gridView().comm().min(minFaceArea);
            minEdgeLength = this->gridView().comm().min(minEdgeLength);
        }


        if(this->gridView().comm().rank() == 0)
        {
            std::cout<<"No of scv for injection: "<<injNoNodes<<std::endl;
            std::cout<<"Injection Cell Volume: "<<injVolume_<<std::endl;
            std::cout<<"Minimal Element Volume: "<<minElementVolume<<" Element Center Coor: "<<minElementCenter<<std::endl;
            std::cout<<"Minimal Face Area: "<<minFaceArea<<" Element Center Coor: "<<minFaceElementCenter<<std::endl;
            std::cout<<"Minimal Edge Length: "<<minEdgeLength<<" Element Center Coor: "<<minEdgeElementCenter<<std::endl;
        }

        //We want to have the first episode end the length of postInitializationTimeStepSize_ before the end of the initialization period
        this->timeManager().startNextEpisode(initializationPeriod_ - postInitializationTimeStepSize_);
}

    void addOutputVtkFields()
    {
        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<double, dimWorld> > VectorField;
        typedef Dune::BlockVector<Dune::FieldVector<int, 1> > IntField;
        // get the number of degrees of freedom
        unsigned numDofs = this->model().numDofs();
        unsigned numElements = this->gridView().size(0);

        //create required scalar fields
        ScalarField *Kxx = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *Kzz = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *cellPorosity = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *boxVolume = this->resultWriter().allocateManagedBuffer(numDofs);
        VectorField *cellCenter = this->resultWriter().template allocateManagedBuffer<double, dimWorld>(numElements);
        ScalarField *fluxLayerBase = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *fluxLayer = this->resultWriter().allocateManagedBuffer(numElements);
        IntField *fluxLine = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        ScalarField *elementVolume = this->resultWriter().allocateManagedBuffer(numElements);
        IntField *layerIdx = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *conform = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        ScalarField *deltaPw = this->resultWriter().allocateManagedBuffer(numDofs);
        ScalarField *temperature = this->resultWriter().allocateManagedBuffer(numDofs);
        IntField *river = this->resultWriter().template allocateManagedBuffer<int>(numDofs);
        (*boxVolume) = 0;
        (*conform) = 1;
        //Fill the scalar fields with values
        ScalarField *rank = this->resultWriter().allocateManagedBuffer(numElements);

        FVElementGeometry fvGeometry;
        VolumeVariables volVars;
        PrimaryVariables initialValues(0);
        GlobalPosition globalPos(0);
        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                int eIdx = this->elementMapper().index(*eIt);
                (*rank)[eIdx] = this->gridView().comm().rank();
                fvGeometry.update(this->gridView(), *eIt);


                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    int dofIdxGlobal = this->model().dofMapper().subIndex(*eIt, scvIdx, dofCodim);
                    globalPos = eIt->geometry().corner(scvIdx);
                    (*boxVolume)[dofIdxGlobal] += fvGeometry.subContVol[scvIdx].volume;
                    (*deltaPw)[dofIdxGlobal] = this->model().curSol()[dofIdxGlobal][pressureIdx] - initialPressureMap_.find(dofIdxGlobal)->second;
//                    initial_(initialValues, globalPos);
//                    (*deltaPw)[dofIdxGlobal] = this->model().curSol()[dofIdxGlobal][pressureIdx] - initialValues[pressureIdx];
                    (*temperature)[dofIdxGlobal] = temperatureAtPos(globalPos);
                    (*river)[dofIdxGlobal] = this->spatialParams().isRiver(*eIt, scvIdx);
                }
                (*Kxx)[eIdx] = this->spatialParams().intrinsicPermeability(*eIt, fvGeometry, /*element data*/ 0)[0][0];
                (*Kzz)[eIdx] = this->spatialParams().intrinsicPermeability(*eIt, fvGeometry, /*element data*/ 0)[dimWorld-1][dimWorld-1];
                (*elementVolume)[eIdx] = eIt->geometry().volume();
                (*layerIdx)[eIdx] = this->spatialParams().findLayerIdx(*eIt);
                int dofIdxGlobal = this->model().dofMapper().subIndex(*eIt, 0, dofCodim);
                (*cellPorosity)[eIdx] = this->spatialParams().porosity(*eIt, fvGeometry, /*element data*/ 0, this->model().curSol()[dofIdxGlobal][pressureIdx]);

                (*cellCenter)[eIdx] = eIt->geometry().center();
                (*fluxLayerBase)[eIdx] = fluxLayerBase_[eIdx];
                (*fluxLayer)[eIdx] = fluxLayer_[eIdx];
                (*fluxLine)[eIdx] = this->spatialParams().findFluxLineIdx(*eIt);
                //Loop over all intersections of the grid and return true if they are conforming
                IntersectionIterator isIt = this->gridView().ibegin(*eIt);
                IntersectionIterator isItEnd = this->gridView().iend(*eIt);
                for(; isIt != isItEnd; ++isIt)
                {
                    if(!isIt->conforming())
                        conform[eIdx] = 0;
                }
            }
        }

        //pass the scalar fields to the vtkwriter
        this->resultWriter().attachDofData(*Kxx, "Kxx", false); //element data
        this->resultWriter().attachDofData(*Kzz, "Kzz", false); //element data
        this->resultWriter().attachDofData(*cellPorosity, "cellwisePorosity", false); //element data
        this->resultWriter().attachDofData(*layerIdx, "layer_index", false); //element data
        this->resultWriter().attachDofData(*boxVolume, "boxVolume", isBox);
        this->resultWriter().attachDofData(*cellCenter, "cellCenter", false, dimWorld); //element data
        this->resultWriter().attachDofData(*elementVolume, "elementVolume", false);
        this->resultWriter().attachDofData(*fluxLayerBase, "fluxLayerBase", false);
        this->resultWriter().attachDofData(*fluxLayer, "fluxLayer", false);
        this->resultWriter().attachDofData(*fluxLine, "fluxLine", false);
        this->resultWriter().attachDofData(*conform, "conform", false);
        this->resultWriter().attachDofData(*deltaPw, "deltaPw", isBox);
        this->resultWriter().attachDofData(*temperature, "temperature", isBox);
        this->resultWriter().attachDofData(*river, "river", isBox);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns true if the current solution should be written to
     *        disk (i.e. as a VTK file)
     *
     * The default behaviour is to write out every the solution for
     * very time step. This file is intented to be overwritten by the
     * implementation.
     */
    bool shouldWriteOutput() const
    {
        if(this->timeManager().episodeWillBeOver() && episodeOutput_)
        {
            return true;
        }
        else
        {
            return  (this->timeManager().timeStepIndex() == 0) ||
                    Dune::FloatCmp::eq<Scalar>(this->timeManager().time(), tInjectionEnd_ + initializationPeriod_) ||
                    this->timeManager().willBeFinished();
        }

    }

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     *
     * The default behaviour is to write one restart file every 5 time
     * steps. This file is intented to be overwritten by the
     * implementation.
     */
    bool shouldWriteRestartFile() const
    {
    	return (this->timeManager().episodeIsOver()) || this->timeManager().willBeFinished();
    }


    /*!
       * \brief Called directly after the time integration.
       */

      void preTimeStep()
      {
          //Set the initial time step size to almost cover the first episode and increase the endTime by the initialization period
          if(this->timeManager().timeStepIndex() == 0)
          {
              Scalar endTime = GET_RUNTIME_PARAM(TypeTag, Scalar, TimeManager.TEnd);
              this->timeManager().setEndTime(endTime + initializationPeriod_);
              this->timeManager().setTimeStepSize(initializationPeriod_ - 2*postInitializationTimeStepSize_);
          }
      }




    /*!
     * \brief Called directly after the time integration.
     */
      void postTimeStep()
      {


          // Calculate storage terms
          PrimaryVariables storage;

          this->model().globalStorage(storage);


          double time = this->timeManager().time();
          double dt = this->timeManager().timeStepSize();
          // Write mass balance information for rank 0
          if (this->gridView().comm().rank() == 0)
          {               // std::setprecision(10);
              std::cout<<"Time: "<<time+dt <<";"<< " Time step size: "<< dt <<";"<<std::endl;
              std::cout<<"Storage: "<< storage <<";"<< std::endl;
          }

          if(injGlobalIdx_[0] != -1)
          {
              std::cout<<"Injection delta Pw: "<<this->model().curSol()[injGlobalIdx_[0]][pressureIdx]<<";"<<std::endl; //- initialPressureMap_.find(injGlobalIdx_[0])->second<<std::endl;
          }

          if(m1GlobalIdx_ != -1)
          {
              std::cout<<"Measurement Point 1 delta Pw: "<<this->model().curSol()[m1GlobalIdx_][pressureIdx]<<";"<<std::endl; //- initialPressureMap_.find(m1GlobalIdx_)->second<<std::endl;
          }
          if(m2GlobalIdx_ != -1)
          {
              std::cout<<"Measurement Point 2 delta Pw: "<<this->model().curSol()[m2GlobalIdx_][pressureIdx]<<";"<<std::endl; //- initialPressureMap_.find(m2GlobalIdx_)->second<<std::endl;
          }

          //Calculate the flux
          ElementIterator eIt = this->gridView().template begin<0>();
          ElementIterator elemEndIt = this->gridView().template end<0>();
          PrimaryVariables fluxAcrossFault(0);
          PrimaryVariables fluxIntoTer(0);
          PrimaryVariables fluxAcrossRupel(0);
          PrimaryVariables fluxAcrossHolesRupel(0);
          PrimaryVariables boundaryFlux(0);

          //Maximum change between old an new time step
          Scalar maxDiffPres = -1e10; //initialize with small number
          GlobalPosition maxDiffPresGlobalPos(0);
          Scalar maxDiffSaltMassFrac = -1e10; //initialize with small number
          GlobalPosition maxDiffSaltMassFracGlobalPos(0);

          for (; eIt != elemEndIt; ++eIt)
          {
              //Calculate maximum change between old and new time step
              FVElementGeometry fvElemGeom;
              fvElemGeom.update(this->gridView(), *eIt);
              for (int scvIdx = 0; scvIdx < fvElemGeom.numScv; ++scvIdx)
              {
                  GlobalPosition globalPos(0);
                  int dofIdxGlobal = this->model().dofMapper().subIndex(*eIt, scvIdx, dofCodim);
                  if(isBox)
                      globalPos = eIt->geometry().corner(scvIdx);
                  else
                      globalPos = eIt->geometry().center();

                  Scalar diffPres = std::abs(this->model().curSol()[dofIdxGlobal][pressureIdx] - this->model().prevSol()[dofIdxGlobal][pressureIdx]);
                  maxDiffPres = std::max(diffPres, maxDiffPres);
                  maxDiffPresGlobalPos = maxDiffPres == diffPres ? globalPos : maxDiffPresGlobalPos;

                  Scalar diffSaltMassFrac = std::abs(this->model().curSol()[dofIdxGlobal][massOrMoleFracIdx] - this->model().prevSol()[dofIdxGlobal][massOrMoleFracIdx]);
                  maxDiffSaltMassFrac = std::max(diffSaltMassFrac, maxDiffSaltMassFrac);
                  maxDiffSaltMassFracGlobalPos = maxDiffSaltMassFrac == diffSaltMassFrac ? globalPos : maxDiffSaltMassFracGlobalPos;

              }
              //Element center
              GlobalPosition globalPos = eIt->geometry().center();
              //check if element marked as a flux element
              if(this->spatialParams().findFluxLineIdx(*eIt) == SpatialParams::rupelFluxIdx)
              {
                  PrimaryVariables flux(0);
                  Scalar zNormalComp = -1e10;
                  IntersectionIterator isIt = this->gridView().ibegin(*eIt);
                  IntersectionIterator isFluxPtr = isIt;
                  IntersectionIterator isEndIt = this->gridView().iend(*eIt);
                  for (; isIt != isEndIt; ++isIt)
                  {
                      if (!isIt->neighbor())
                          continue;

                      //Use the intersection with the highest positive z value of its normal, i.e. normal pointing in vertical direction
                      zNormalComp = std::max(zNormalComp, isIt->centerUnitOuterNormal()[dimWorld-1]);
                      isFluxPtr = zNormalComp > isIt->centerUnitOuterNormal()[dimWorld-1] ? isFluxPtr : isIt;
                  }
                  //Calculate the flux across the intersection
                  calculateFluxAccrossIntersection_(flux, *eIt, *isFluxPtr);

                  //If the flux element is not part of the rupel layer it must be the fault zone
                  if(this->spatialParams().findLayerIdx(*eIt) != SpatialParams::rcIdx)
                  {
                      fluxAcrossFault += flux;
                  }
                  if(this->spatialParams().findLayerIdx(*eIt) == SpatialParams::rcIdx)
                  {
                      fluxAcrossRupel += flux;
                  }
                  if(globalPos[0] < 730851.6875 && this->spatialParams().findLayerIdx(*eIt) != SpatialParams::rcIdx)
                  {
                      fluxAcrossHolesRupel += flux;
                  }

                  //make the flux specific to the surface kg/s/m2
                  fluxIntoTer += flux;
                  flux /= isFluxPtr->geometry().volume();
                  fluxLayer_[this->elementMapper().index(*eIt)] = flux[0];

                  if(time+dt == initializationPeriod_)
                  {
                      fluxLayerBase_[this->elementMapper().index(*eIt)] = flux[0];
                  }
              }



              IntersectionIterator isIt = this->gridView().ibegin(*eIt);
              IntersectionIterator isEndIt = this->gridView().iend(*eIt);
              if(this->spatialParams().findLayerIdx(*eIt) == SpatialParams::quarIdx)
              {
                  for (; isIt != isEndIt; ++isIt)
                  {
                      if(isIt->boundary() && Dune::FloatCmp::lt<Scalar>(isIt->centerUnitOuterNormal()[dimWorld-1], -0.2))
                      {
                          calculateFluxAccrossIntersection_(boundaryFlux, *eIt, *isIt);
                          //make the flux specific to the surface kg/s/m2
                          boundaryFlux /= isIt->geometry().volume();
                          fluxLayer_[this->elementMapper().index(*eIt)] = boundaryFlux[0];
                          if(Dune::FloatCmp::eq<Scalar>(time+dt, initializationPeriod_))
                              fluxLayerBase_[this->elementMapper().index(*eIt)] = boundaryFlux[0];
                      }
                  }
              }


          }


          if (this->gridView().comm().size() > 1)
          {
              //sum the flux and storage over processes if parallel
              fluxAcrossFault = this->gridView().comm().sum(fluxAcrossFault);
              fluxAcrossRupel = this->gridView().comm().sum(fluxAcrossRupel);
              fluxAcrossHolesRupel = this->gridView().comm().sum(fluxAcrossHolesRupel);
              fluxIntoTer = this->gridView().comm().sum(fluxIntoTer);
              boundaryFlux = this->gridView().comm().sum(boundaryFlux);
          }

          if (this->gridView().comm().rank() == 0)
          {
              //Write the maximum change between old an new time step
              std::cout<<"Maximum difference between last and current time step: "<<maxDiffPres<<";"<<std::endl;
              std::cout<<"Located at: "<<maxDiffPresGlobalPos<<";"<<std::endl;
              std::cout<<"Maximum difference between last and current time step: "<<maxDiffSaltMassFrac<<";"<<std::endl;
              std::cout<<"Located at: "<<maxDiffSaltMassFracGlobalPos<<";"<<std::endl;


              //Write the flux
              std::cout<<"Flux Across Fault (holes in Rupel and saltwall): Total Flux: "
                      <<fluxAcrossFault[contiWEqIdx]<<";"<<" Salt Flux: "<<fluxAcrossFault[transportEqIdx]<<";"<<std::endl;
              std::cout<<"Flux Across Rupel: Total Flux: "<<fluxAcrossRupel[contiWEqIdx]<<";"<<" Salt Flux: "<<fluxAcrossRupel[transportEqIdx]<<";"<<std::endl;
              std::cout<<"Flux Across Holes Rupel: Total Flux: "<<fluxAcrossHolesRupel[contiWEqIdx]<<";"
                      <<" Salt Flux: "<<fluxAcrossHolesRupel[transportEqIdx]<<";"<<std::endl;
              std::cout<<"Flux into Ter Quar: Total Flux: "<<fluxIntoTer[contiWEqIdx]<<";"<<" Salt Flux: "<<fluxIntoTer[transportEqIdx]<<";"<<std::endl;
          }



          //Set pressure and salt mass fraction values for side boundaries after initialization period
          if(Dune::FloatCmp::eq<Scalar>(time+dt, initializationPeriod_))
          {
              if (this->gridView().comm().rank() == 0)
              {
                  std::cout<<"Assigning Dirichlet boundary conditions for side boundaries"
                          "from the solution after the initialization period"<<std::endl;
              }
              FVElementGeometry fvGeometry;
              ElementIterator eIt = this->gridView().template begin<0>();
              ElementIterator elemEndIt = this->gridView().template end<0>();

              for (; eIt != elemEndIt; ++eIt)
              {
                  fvGeometry.update(this->gridView(), *eIt);

                  for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                  {
                      //                          int globalIdx = this->model().dofMapper().map(*eIt, scvIdx, dofCodim);
                      //                          GlobalPosition globalPos(0);
                      int dofIdxGlobal = 0;
                      if(GET_PROP_VALUE(TypeTag, ImplicitIsBox))
                      {
                          //                              globalPos = eIt->geometry().corner(scvIdx);
                          dofIdxGlobal = this->vertexMapper().subIndex(*eIt, scvIdx, dofCodim);
                      }
                      else
                      {
                          //                              globalPos = eIt->geometry().center();
                          dofIdxGlobal = this->elementMapper().subIndex(*eIt, scvIdx, dofCodim);
                      }
                      //store the pressure after initialization for each dof, this is needed for the boundary conditions (lateral boundaries)
                      //and the porosity calculation (reference pressure)
                      initialPressureMap_[dofIdxGlobal] = this->model().curSol()[dofIdxGlobal][pressureIdx];

                      //Check whether the dof is situated at a lateral boundary, we only need dofs at the lateral boundary for this map
                      if(dofAtLateralBoundary_(*eIt, scvIdx))
                      {
                          initialSalinityMapAtBoundary_.insert(std::make_pair(dofIdxGlobal, this->model().curSol()[dofIdxGlobal][massOrMoleFracIdx]));
                      }
                  }
              }
          }
      }


      void episodeEnd()
      {
          //Last episode in initialization period
          if(Dune::FloatCmp::lt<Scalar>(this->timeManager().time(), initializationPeriod_))
          {
              this->timeManager().startNextEpisode(initializationPeriod_ - this->timeManager().time());
              std::cout<<"Episode index is set to: "<<this->timeManager().episodeIndex()<<std::endl;
          }
          // Start injection episodes
          else if(Dune::FloatCmp::lt<Scalar>(this->timeManager().time(), tInjectionEnd_ + initializationPeriod_))
          {
              this->timeManager().startNextEpisode(injectionEpisodeLength_);
              std::cout<<"Episode index is set to: "<<this->timeManager().episodeIndex()<<std::endl;
              //Set the time step size after initialization
              if(this->timeManager().episodeIndex() == 3)
                  this->timeManager().setTimeStepSize(postInitializationTimeStepSize_);
          }
          // Start post injection episodes
          else
          {
              this->timeManager().startNextEpisode(postInjectionEpisodeLength_);
              std::cout<<"Episode index is set to: "<<this->timeManager().episodeIndex()<<std::endl;
              if(Dune::FloatCmp::eq<Scalar>(this->timeManager().time(), tInjectionEnd_ + initializationPeriod_))
                  std::cout<<"Injection has stopped."<<std::endl;
          }
      }



    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    {
    	return name_;
    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     *
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        Scalar temperature;
        if(applyGeothermalGradient_)
            temperature = 273.15 + 8. + 0.03*(topOfReservoir_ + globalPos[dimWorld-1]);
        else
            temperature = 273.15 + 8. + 0.03*(topOfReservoir_ + zInjCoord_);

        if(temperature < 273.15 + 8.)
            temperature = 273.15 + 8.;

        return temperature;
    }


    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvElemGeom,
                int scvIdx) const
    {
        values = 0;

        // injection implementation
        if(Dune::FloatCmp::ge<Scalar>(this->timeManager().time(), initializationPeriod_)
                && Dune::FloatCmp::lt(this->timeManager().time(), tInjectionEnd_ + initializationPeriod_))
        {
            GlobalPosition globalPos(0);
            if(isBox)
            {
                globalPos = element.geometry().corner(scvIdx);
                //                    globalIdx = this->vertexMapper().map(element, scvIdx, dofCodim);
            }
            else
            {
                globalPos = element.geometry().center();
                //                    globalIdx = this->elementMapper().map(element, scvIdx, dofCodim);
            }

            if(isInjectionCell_(globalPos))
            {
                values[wPhaseIdx] = injectionRate_/injVolume_;
                values[transportEqIdx] = injectionRate_/injVolume_*1e-6;
            }
        }

        // define constant salt mass fraction for salt wall
        if(enableSaltTransport_ && this->spatialParams().findLayerIdx(element) == SpatialParams::zecIdx)
        {
            int globalIdx = this->vertexMapper().subIndex(element, scvIdx, dofCodim);
            bool vAtSaltBoundary = this->spatialParams().vertexAtSaltBoundary(globalIdx);
            if((isBox && !vAtSaltBoundary) || !isBox)
            {
                values[transportEqIdx] = maxNaClMassFrac_;
            }
        }
    }



    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex on the boundary for which the
     *               conditions needs to be specified
     */
   void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        intersectionToVertexBC_.boundaryTypes(values, vertex);
    }

    void boundaryTypes(BoundaryTypes &values, const Intersection &is) const
    {
            values.setAllNeumann();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        if(topAquiferMissing_)
        {
            initial_(values, globalPos);
        }
        else
        {
            values[pressureIdx] = 1e5;
            values[transportEqIdx] = 0.;
        }
    }



    /*!
      * \brief Evaluate the boundary conditions for a neumann
      *        boundary segment.
      *
      * This is the method for the case where the Neumann condition is
      * potentially solution dependent and requires some quantities that
      * are specific to the fully-implicit method.
      *
      * \param values The neumann values for the conservation equations in units of
      *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
      * \param element The finite element
      * \param fvGeometry The finite-volume geometry
      * \param intersection The intersection between element and boundary
      * \param scvIdx The local subcontrolvolume index
      * \param boundaryFaceIdx The index of the boundary face
      * \param elemVolVars All volume variables for the element
      *
      * For this method, the \a values parameter stores the mass flux
      * in normal direction of each phase. Negative values mean influx.
      */
     void solDependentNeumann(PrimaryVariables &values,
                       const Element &element,
                       const FVElementGeometry &fvGeometry,
                       const Intersection &is,
                       const int scvIdx,
                       const int boundaryFaceIdx,
                       const ElementVolumeVariables &elemVolVars) const
     {
         values = 0;

         const GlobalPosition normal = is.centerUnitOuterNormal();
         const GlobalPosition globalPos = fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
         Scalar normalZComp, absNormalZComp;
         normalZComp = normal[dimWorld-1];
         absNormalZComp = std::fabs(normalZComp);


         //Top boundary condition
         if(Dune::FloatCmp::gt<Scalar>(absNormalZComp, 0.2) && Dune::FloatCmp::lt<Scalar>(normalZComp, 0))
         {

             bool isRiver = this->spatialParams().isRiver(element, scvIdx);
             if(dirichletAtTop_ || isRiver)
             {
                 values[pressureIdx] = elemVolVars[scvIdx].pressure() - pressureTOR_; // [Pa]
             }
             else
             {
                 values[pressureIdx] = -gWRegenerationRate_; //[kg/sq/s]
             }
         }


         //Lateral boundary condition
         if(Dune::FloatCmp::ge<Scalar>(this->timeManager().time(), initializationPeriod_) && Dune::FloatCmp::le<Scalar>(absNormalZComp, 0.2) && !closed_)
         {
             int dofIdxGlobal = 0;
             if(GET_PROP_VALUE(TypeTag, ImplicitIsBox))
             {
                 dofIdxGlobal = this->vertexMapper().subIndex(element, scvIdx, dofCodim);
             }
             else
             {
                 dofIdxGlobal = this->elementMapper().subIndex(element, scvIdx, dofCodim);
             }
             values[pressureIdx] = elemVolVars[scvIdx].pressure() - initialPressureMap_.find(dofIdxGlobal)->second;
             values[transportEqIdx] = elemVolVars[scvIdx].massFraction(massOrMoleFracIdx) - initialSalinityMapAtBoundary_.find(dofIdxGlobal)->second;
         }

         //Set no flow boundary conditions for the symmetry axes of the generic models
#if(GEOMETRY == 1)
         if(globalPos[1] < this->bBoxMin()[1] + eps_)
             values = 0;
#endif
#if(GEOMETRY == 2) //two layer system have no flow boundaries for xmin and ymin
         if(globalPos[0] < this->bBoxMin()[0] + eps_ || globalPos[1] < this->bBoxMin()[1] + eps_)
             values = 0;
#endif
         //Set Dirichlet conditions at the top during the initialization for the generic models
#if(GEOMETRY != 0)
         if(Dune::FloatCmp::lt<Scalar>(this->timeManager().time(), initializationPeriod_)
                 && Dune::FloatCmp::gt<Scalar>(absNormalZComp, 0.2) && Dune::FloatCmp::lt<Scalar>(normalZComp, 0))
             values[pressureIdx] = elemVolVars[scvIdx].pressure() - pressureTOR_; // [Pa]
#endif

     }
    // \}


    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvElemGeom,
                 int scvIdx) const
    {
        GlobalPosition globalPos;
        globalPos = 0;

        //Where does the primary variable live?
        if(isBox)
            globalPos = element.geometry().corner(scvIdx);
        else
            globalPos = element.geometry().center();

        // Set the initial pressure
        initial_(values, globalPos);

        //Set the initial salt concentration
        if(enableSaltTransport_)
        {
            if(isBox)
            {
                int dofIdxGlobal = this->vertexMapper().subIndex(element, scvIdx, dofCodim);
                bool vAtTZ = this->spatialParams().vertexAtSaltBoundary(dofIdxGlobal);
                bool vTerQuar = this->spatialParams().vertexTerQuar(dofIdxGlobal);
                // If it is a salt dome box set the maximum salt mass fraction
                if(this->spatialParams().findLayerIdx(element) == SpatialParams::zecIdx && !vAtTZ)
                                    {values[transportEqIdx] = maxNaClMassFrac_;}
                else if(vTerQuar)
                {
                    values[transportEqIdx] = 0.0;
                }
                else
                {
                    values[transportEqIdx] = (globalPos[dimWorld-1] - depthSalt_)*saltGrad_;
                }
            }
            // cell centered
            else
            {
                // If it is a salt dome element set the maximum salt mass fraction
                if(this->spatialParams().findLayerIdx(element) == SpatialParams::zecIdx)
                    {values[transportEqIdx] = maxNaClMassFrac_;}
                else if(this->spatialParams().findLayerIdx(element) == SpatialParams::terIdx ||
                        this->spatialParams().findLayerIdx(element) == SpatialParams::quarIdx)
                    {values[transportEqIdx] = 0.0;}
                else
                {
                    values[transportEqIdx] = (globalPos[dimWorld-1] - depthSalt_)*saltGrad_;
                }
            }
            // Remove negative values and values over maxNaClMassFrac_
            if(values[transportEqIdx] < 0)
                values[transportEqIdx] = 0.0;
            if(values[transportEqIdx] > maxNaClMassFrac_)
                values[transportEqIdx] = maxNaClMassFrac_;
        }
    }

    // Overwrite the default gravity function for problem, gravity_ is set in the constructor
    const GlobalPosition& gravity() const
    {
        return gravity_;
    }

    // the internal method for the initial condition
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        // Set hydrostatic conditions and zero salt mass fraction
        values[pwIdx] = pressureTOR_ + 1000*this->gravity()[dimWorld-1]*(std::abs(this->bBoxMin()[dimWorld-1]) + globalPos[dimWorld-1]);
        values[transportEqIdx] = 0.0;
    }


private:

    bool dofAtLateralBoundary_(const Element &element, int scvIdx) const
    {
        IntersectionIterator isIt = this->gridView().ibegin(element);
        IntersectionIterator isEndIt = this->gridView().iend(element);
        for (; isIt != isEndIt; ++isIt)
        {
            //Check if intersection on boundary
            if(!isIt->boundary())
                continue;

            //Check if intersection on lateral boundary
            Scalar normalZComp = isIt->centerUnitOuterNormal()[dimWorld -1];
            Scalar absNormalZComp = std::fabs(normalZComp);
            if(Dune::FloatCmp::le<Scalar>(absNormalZComp, 0.2))
            {
                //box
                if(isBox)
                {
                    if(scvIdxIsOnIntersection_(scvIdx, *isIt, element))
                        return true;
                }
                //cc
                else
                {
                    return true;
                }
            }
        }
        return false;
    }

    void calculateFluxAccrossIntersection_(PrimaryVariables &flux, const Element &element, const Intersection &is) const
    {
        if(element.partitionType() == Dune::InteriorEntity)
        {
            Scalar massUpwindWeight = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);


            if(!isBox)
            {
                ElementVolumeVariables elemVolVars;
                FVElementGeometry fvElemGeom;
                fvElemGeom.update(this->gridView(), element);
                //Update the element volume variables with the current solution
                elemVolVars.update(*this, element, fvElemGeom, false /* oldSol? */);
                //iterate over all degrees of freedom (i.e. nodes or element centers)
                for (int scvfIdx = 0; scvfIdx < fvElemGeom.numScvf; ++scvfIdx)
                {
                    if(is.indexInInside() == scvfIdx)
                    {
                        FluxVariables vars(*this, element, fvElemGeom, scvfIdx, elemVolVars);
                        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
                        {
                            // of the current phase
                            const VolumeVariables &up =  elemVolVars[vars.upstreamIdx()];
                            const VolumeVariables &dn =  elemVolVars[vars.downstreamIdx()];
                            if(useMoles)
                            {
                                flux[contiWEqIdx] += vars.volumeFlux(phaseIdx)
                                                                 *((massUpwindWeight)*up.molarDensity()+(1 - massUpwindWeight)*dn.molarDensity());
                                flux[transportEqIdx] += vars.volumeFlux(phaseIdx)
                                                            *((massUpwindWeight)*up.molarDensity()*up.moleFraction(massOrMoleFracIdx)
                                                                    +(1 - massUpwindWeight)*dn.molarDensity()*dn.moleFraction(massOrMoleFracIdx));
                            }
                            else
                            {
                                flux[contiWEqIdx] += vars.volumeFlux(phaseIdx)
                                                                *((massUpwindWeight)*up.density()+(1 - massUpwindWeight)*dn.density());
                                flux[transportEqIdx] += vars.volumeFlux(phaseIdx)
                                                            *((massUpwindWeight)*up.density()*up.massFraction(massOrMoleFracIdx)
                                                                    +(1 - massUpwindWeight)*dn.density()*dn.massFraction(massOrMoleFracIdx));
                             }
                        }
                        //jump out of the scvf loop after correct scvfIdx has been found
                        continue;
                    }
                }
            }
            else
            {
                FVElementGeometry fvElemGeom;
                fvElemGeom.update(this->gridView(), element);
                ElementVolumeVariables elemVolVars;
                elemVolVars.update(*this, element, fvElemGeom, false /* oldSol? */);


//                //get the intersection center unit normal
//                GlobalPosition isCenterUnitOuterNormal = is.centerUnitOuterNormal();
                //iterate over all degrees of freedom (i.e. nodes or element centers)
                for (int scvfIdx = 0; scvfIdx < fvElemGeom.numScvf; ++scvfIdx)
                {
//                    GlobalPosition scvfCenterUnitOuterNormal = fvElemGeom.subContVolFace[scvfIdx].normal;
//                    scvfCenterUnitOuterNormal /= fvElemGeom.subContVolFace[scvfIdx].normal.two_norm();
                    PrimaryVariables tmpFlux(0);
                    int scvIdxI = fvElemGeom.subContVolFace[scvfIdx].i;
                    int scvIdxJ = fvElemGeom.subContVolFace[scvfIdx].j;
                    bool iOnIntersection = scvIdxIsOnIntersection_(scvIdxI, is, element);
                    bool jOnIntersection = scvIdxIsOnIntersection_(scvIdxJ, is, element);
                    int multiplier;
                    if(iOnIntersection && !jOnIntersection)
                        multiplier = -1;
                    else if(!iOnIntersection && jOnIntersection)
                        multiplier = 1;
                    else
                        multiplier = 0;


                    if(multiplier != 0)
                    {
                        FluxVariables vars(*this, element, fvElemGeom, scvfIdx, elemVolVars);

                        const VolumeVariables &up =  elemVolVars[vars.upstreamIdx()];
                        const VolumeVariables &dn =  elemVolVars[vars.downstreamIdx()];
                        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
                        {

                            if(useMoles)
                            {
                                tmpFlux[contiWEqIdx] += vars.volumeFlux(phaseIdx)
                                                                     *((massUpwindWeight)*up.molarDensity()+(1 - massUpwindWeight)*dn.molarDensity());
                                tmpFlux[transportEqIdx] += vars.volumeFlux(phaseIdx)
                                                                       *((massUpwindWeight)*up.molarDensity()*up.moleFraction(massOrMoleFracIdx)
                                                                               +(1 - massUpwindWeight)*dn.molarDensity()*dn.moleFraction(massOrMoleFracIdx));
                            }
                            else
                            {
                                tmpFlux[contiWEqIdx] += vars.volumeFlux(phaseIdx)
                                                                      *((massUpwindWeight)*up.density()+(1 - massUpwindWeight)*dn.density());
                                tmpFlux[transportEqIdx] += vars.volumeFlux(phaseIdx)
                                                                       *((massUpwindWeight)*up.density()*up.massFraction(massOrMoleFracIdx)
                                                                               +(1 - massUpwindWeight)*dn.density()*dn.massFraction(massOrMoleFracIdx));
                            }
                        }
                    }
                    tmpFlux *= multiplier;
                    flux += tmpFlux;
                }
            }
        }
    }

    //Check whether scvIdx is on intersection
    bool scvIdxIsOnIntersection_(int scvIdx, const Intersection &is, const Element &element) const
    {
//        const Geometry& geometry = element.geometry();
        Dune::GeometryType geomType = element.geometry().type();
        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
        int intersectionIdx = is.indexInInside();
        int numVerticesOfIntersection = referenceElement.size(intersectionIdx, 1, dim);
        for (int vertInIntersection = 0; vertInIntersection < numVerticesOfIntersection; vertInIntersection++)
        {
            int vertInElementIdx = referenceElement.subEntity(intersectionIdx, 1, vertInIntersection, dim);
            if(vertInElementIdx == scvIdx)
                return true;
        }
        return false;
    }
    
    bool isInjectionCell_(GlobalPosition &globalPos) const
    {

        if(globalPos[0] > xInjCoord_ - deltaX_ && globalPos[0] < xInjCoord_ + deltaX_
                        && globalPos[1] < yInjCoord_ + deltaX_ && globalPos[1] > yInjCoord_ - deltaX_
                        && globalPos[2] < zInjCoord_ + deltaZ_ && globalPos[2] > zInjCoord_ - deltaZ_)
        {return true;}

        return false;
    }


  // General
  Scalar eps_;
  std::string name_ ;
  GlobalPosition gravity_;

  // Episode management
  Scalar initializationPeriod_;
  Scalar tInjectionEnd_;
  Scalar injectionEpisodeLength_;
  Scalar postInjectionEpisodeLength_;
  Scalar postInitializationTimeStepSize_;
  bool episodeOutput_;

  // FluidSystem initialization
  int nTemperature_;
  int nPressure_;
  Scalar pressureLow_, pressureHigh_;
  Scalar temperatureLow_, temperatureHigh_;

// Coordinate boundaries of the injection well
  Scalar xInjCoord_;
  Scalar yInjCoord_;
  Scalar zInjCoord_;
  Scalar deltaZ_;
  Scalar deltaX_;

  //Measurement point vectors
  GlobalPosition m1_;
  GlobalPosition m2_;
  int m1GlobalIdx_;
  int m2GlobalIdx_;

  // Vector with global indices for injection coordinates
  std::vector<int> injGlobalIdx_;

  // Total volume of injection cells
  Scalar injVolume_;
  Scalar injectionRate_;

  // Boundary conditions
public:
  std::map<int, Scalar> initialPressureMap_;
  std::map<int, Scalar> initialSalinityMapAtBoundary_;
private:
  Scalar pressureTOR_;
  Scalar topOfReservoir_;
  Scalar gWRegenerationRate_;
  const IntersectionToVertexBC<TypeTag> intersectionToVertexBC_;
  bool topAquiferMissing_;
  bool closed_;
  bool dirichletAtTop_;
  bool useCO2VolumeEquivalentInjRate_;

  //initial conditions
  Scalar massReductionFactor_;
  bool enableSaltTransport_;
  Scalar saltGrad_;
  Scalar depthSalt_;
  Scalar maxNaClMassFrac_;
  bool applyGeothermalGradient_;

  //flux calcultation elements 1 = true, 0 = false
  std::vector<Scalar> fluxLayerBase_;
  std::vector<Scalar> fluxLayer_;


};
} //end namespace

#endif
