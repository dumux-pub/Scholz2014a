// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outlfow problem.
 */
#ifndef DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH
#define DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH

#if SPATIALPARAMS == 1
#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#elif SPATIALPARAMS == 2
#include <dumux/material/spatialparams/implicitspatialparams.hh>
#else
#include <dumux/material/spatialparams/fvspatialparams.hh>
#endif
//#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dune/common/float_cmp.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class SollingSpatialParams;

namespace Properties
{

#if SPATIALPARAMS != 1
// The spatial parameters TypeTag
NEW_TYPE_TAG(SollingSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(SollingSpatialParams, SpatialParams, Dumux::SollingSpatialParams<TypeTag>);
// Set the material Law
SET_PROP(SollingSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};

#endif
}
/*!
 * \ingroup OnePTwoCBoxModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outflow problem.
 */
template<class TypeTag>
#if SPATIALPARAMS == 1
class SollingSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
#elif SPATIALPARAMS == 2
class SollingSpatialParams : public ImplicitSpatialParams<TypeTag>
#else
class SollingSpatialParams : public FVSpatialParams<TypeTag>
#endif
{
    // Parent Type
#if SPATIALPARAMS == 1
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;
#elif SPATIALPARAMS == 2
    typedef ImplicitSpatialParams<TypeTag> ParentType;
#else
    typedef FVSpatialParams<TypeTag> ParentType;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

    typedef typename GridView:: template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;


    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        pressureIdx = Indices::pressureIdx,
    };
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;
  //  typedef Dune::ReservoirPropertyCapillary<3> ReservoirProperties;
    typedef typename GET_PROP(TypeTag, ParameterTree) Params;

    typedef std::vector<FieldMatrix> PermeabilityType;
#if SPATIALPARAMS != 1
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    //typedef LinearMaterial<Scalar> EffMaterialLaw;
public:

    enum {
        //Layer indices
        quarIdx=0,
        rcIdx=1,
        solIdx=2,
        tzIdx=3,
        terIdx = 4,
        zecIdx=5,

        //Flux line indices
        noFluxIdx=0,
        rupelFluxIdx=1,
    };


    SollingSpatialParams(const GridView &gridView)
        : ParentType(gridView), gridView_(gridView)
    {

        Scalar initialPermeability(0);
        try
        {
            topOfReservoir_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TopOfReservoir);
            kzByKx_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.KzByKx);
            Swr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Swr);
            Snr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Snr);
            brooksCoreyPe_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyPe);
            brooksCoreyLambda_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyLambda);
            compressibility_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Compressibility);
            initialPermeability = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.InitialPermeability);
            arrayPerm_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermQuar);
            arrayPerm_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermRC);
            arrayPerm_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermSol);
            arrayPerm_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermTZ);
            arrayPor_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorQuar);
            arrayPor_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorRC);
            arrayPor_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorSol);
            arrayPor_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorTZ);
            arrayBotLayer_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotQuar);
            arrayBotLayer_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotRC);
            arrayBotLayer_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotSol);
            tzWidth_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.tzWidth);
            dz_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Dz);

            //get the initialization period
            initializationPeriod_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InitializationPeriod);
            //get the mass reduction factor
            massReductionFactor_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MassReductionFactor);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the spatial parameters file!\n";
            exit(1);
        }

        //initial permeability
//        initialPermeability_ = Scalar(0.0); //initialise the permeability tensor
        for(int i=0; i<dimWorld; ++i)
        {
            //High permeability for initialization
            initialPermeability_[i][i]  = initialPermeability;
        }


#if SPATIALPARAMS != 1
        // residual saturations
        materialParams_.setSwr(Swr_);
        materialParams_.setSnr(Snr_);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(brooksCoreyPe_);
        materialParams_.setLambda(brooksCoreyLambda_);
#endif
    }

    ~SollingSpatialParams()
    {}

    void setParams(GridPointer *gridPtr, Problem *problemPtr)
    {
        gridPtr_ = gridPtr;
        problemPtr_ = problemPtr;

        int numElems = gridView_.size(0);

        permloc_.resize(numElems);
        porosity_.resize(numElems);
        ElementIterator elemIt = gridView_.template begin<0>();
        const ElementIterator elemEndIt = gridView_.template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {

            int elemIdx = gridView_.indexSet().index(*elemIt);
            int layerIdx = findLayerIdx(*elemIt);
            if(layerIdx < 0)
                layerIdx=0;

            porosity_[elemIdx] = arrayPor_[layerIdx];

//            if (porosity_[elemIdx]<=1e-3)
//            { porosity_[elemIdx]=1e-3;}


            permloc_[elemIdx] = Scalar(0.0); //initialise the permeability tensor
            for(int i=0; i<dim; ++i)
            {

                permloc_[elemIdx][i][i] = arrayPerm_[layerIdx];

                //If permloc is smaller than the smallest permeability defined in arrayPerm than assign the smallest arrayPerm to permloc
//                permloc_[elemIdx][i][i] = permloc_[elemIdx][i][i] < 1e-20 ? 1e-20 : permloc_[elemIdx][i][i];

            }
            permloc_[elemIdx][dimWorld-1][dimWorld-1]=permloc_[elemIdx][dimWorld-1][dimWorld-1]*kzByKx_;
        }

        Scalar maxPor = 0;
        Scalar minPor = 0;
        minPor = *std::min_element(porosity_.begin(), porosity_.end());
        maxPor = *std::max_element(porosity_.begin(), porosity_.end());


        Scalar maxPerm = 0.;
        Scalar minPerm = 1e10;

        for (auto & element : permloc_)
        {
            for(int i=0; i<dim; ++i)
            {
                maxPerm = element[i][i] > maxPerm ? element[i][i] : maxPerm;
                minPerm = element[i][i] < minPerm ? element[i][i] : minPerm;
            }
        }

        if (gridView_.comm().size() > 1)
        {
            minPor = gridView_.comm().min(minPor);
            maxPor = gridView_.comm().min(maxPor);
            maxPerm = gridView_.comm().min(maxPerm);
            minPerm = gridView_.comm().min(minPerm);
        }

        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Minimum Perm: "<<minPerm<<" Maximum Perm: "<<maxPerm<<std::endl;
            std::cout<<"Minimum Porosity: "<<minPor<<" Maximum Porosity: "<<maxPor<<std::endl;
        }
    }

    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution the global solution vector
     */
//    void update(const SolutionVector &globalSolution)
//    {
//    };

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
#if SPATIALPARAMS != 3
    const FieldMatrix& intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvElemGeom,
                                 int scvIdx) const
#else
    const FieldMatrix& intrinsicPermeability(const Element &element) const
#endif
    {
        int elemIdx = gridView_.indexSet().index(element);
        if(Dune::FloatCmp::lt<Scalar>(problemPtr_->timeManager().time(), initializationPeriod_) &&
                Dune::FloatCmp::lt<Scalar>(permloc_[elemIdx][0][0], initialPermeability_[0][0]))
        {
            return initialPermeability_; //high permeability for creating hydrostatic conditions
        }

        return permloc_[elemIdx];
    }

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
        return 0;
    }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
#if SPATIALPARAMS != 3
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx,
                    Scalar pressure) const
#else
    Scalar porosity(const Element &element) const
#endif
    {
//        if(problemPtr_->timeManager().time() < initializationPeriod_)
//        {
//            return initializationPeriodPor_;
//        }

        int dofIdx = 0;
        if(GET_PROP_VALUE(TypeTag, ImplicitIsBox))
        {
            dofIdx = problemPtr_->vertexMapper().subIndex(element, scvIdx, dofCodim);
        }
        else
        {
            dofIdx = problemPtr_->elementMapper().subIndex(element, scvIdx, dofCodim);
        }
        int elementIdx = gridView_.indexSet().index(element);

        //effective porosity calculation according to Schäfer et al. 2011
        Scalar refPorosity = porosity_[elementIdx];
        if(problemPtr_->timeManager().episodeIndex() == 1)
        {
            refPorosity *= massReductionFactor_;
            return refPorosity;
        }
//        Scalar compressibilty = 4.5e-10; // 1/Pa
        Scalar refPressure = problemPtr_->initialPressureMap_.find(dofIdx)->second;
        Scalar X = compressibility_*(pressure - refPressure);
        Scalar effPorosity = refPorosity*(1 + X + std::pow(X,2)/2);
        return effPorosity;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
#if SPATIALPARAMS == 2
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
#elif SPATIALPARAMS == 3
    const MaterialLawParams& materialLawParams(const Element &element) const
#endif
#if SPATIALPARAMS != 1
    {
        return materialParams_;
    }
#endif

    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    Scalar dispersivity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        return 0;
    }

    bool useTwoPointGradient(const Element &element,
                             const int vertexI,
                             const int vertexJ) const
    {
        return false;
    }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar heatCapacity(const Element &element,
//                        const FVElementGeometry &fvGeometry,
//                        const int scvIdx) const
//    {
//        return
//            790 // specific heat capacity of granite [J / (kg K)]
//            * 2700 // density of granite [kg/m^3]
//            * (1 - porosity(element, fvGeometry, scvIdx));
//    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/m^2]\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar thermalConductivitySolid(const Element &element,
//                                    const FVElementGeometry &fvGeometry,
//                                    const int scvIdx) const
//    {
//        return lambdaSolid_;
//    }

    //Check if the vertex is at the transition zone, i.e. having both zec and other layer properties
    bool vertexAtSaltBoundary(const int vIdxGlobal) const
    {
        return false;
    }

    //Find the dof belonging to a river
    bool isRiver(const Element &element, int vIdx) const
    {
        return false;
    }

    //Check if the vertex has ter or quar layer neighbors (internal function)
    bool vertexTerQuar(const int vIdxGlobal) const
    {
        return false;
    }

    //Find the elements belonging to the flux line
    int findFluxLineIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        if(globalPos[dim-1] < arrayBotLayer_[rcIdx] && globalPos[dim-1] > arrayBotLayer_[quarIdx] + dz_)
            return rupelFluxIdx;
        else
            return noFluxIdx;

    }

    //Find the layer index from the grid file
    int findLayerIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        Scalar xMin = problemPtr_->bBoxMin()[0];
        if(globalPos[0] < xMin + tzWidth_)
            return tzIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[quarIdx])
            return quarIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[rcIdx] && globalPos[dim-1] > arrayBotLayer_[quarIdx])
            return rcIdx;
        else
            return solIdx;
    }


private:




    static constexpr int numLayer_ = 4;
    Scalar arrayBotLayer_[numLayer_];
    Scalar saltDomeWidth_;
    Scalar topOfReservoir_;

    PermeabilityType permloc_;
    PermeabilityType permloc_high_;
    std::vector<double> porosity_;
    Scalar tzWidth_;
    Scalar dz_;
    mutable FieldMatrix initialPermeability_;

   // ReservoirProperties* resProps_;
    const GridView gridView_;
    Scalar kzByKx_;
    GridPointer *gridPtr_;

    Scalar arrayPerm_[numLayer_];
    Scalar arrayPor_[numLayer_];

    //Two phase parameters
#if SPATIALPARAMS != 1
    MaterialLawParams materialParams_;
#endif
    Scalar Swr_;
    Scalar Snr_;
    Scalar brooksCoreyPe_;
    Scalar brooksCoreyLambda_;
    Scalar compressibility_;

    // problem pointer for obtaining the solution
    Problem *problemPtr_;

    //Initialization period
    Scalar initializationPeriod_;

    //Mass reduction factor for multiplication with porosity
    Scalar massReductionFactor_;

    //key: element index, value: is fault element?
    std::map<int, bool> elementFaultMap_;
};

}

#endif
