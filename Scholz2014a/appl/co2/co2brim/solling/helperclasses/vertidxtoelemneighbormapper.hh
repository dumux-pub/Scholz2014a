// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_VERTIDXTOELEMNEIGHBORMAPPER_HH
#define DUMUX_VERTIDXTOELEMNEIGHBORMAPPER_HH

#include<vector>
#include<unordered_map>
#include<dune/grid/common/mcmgmapper.hh>
#include<dune/grid/common/grid.hh>

/**
 * @file
 * @brief  defines a vertex mapper for mapping neighbor elements to a global vertex index. Every element in which a vertex appears is a neighbor element.
 */

namespace Dumux
{

template<class GridView>
class VertIdxToElemNeighborMapper
{

    typedef typename GridView::Grid Grid;
    enum {dim=Grid::dimension};
    typedef typename Grid::template Codim<0>::Entity Element;
    typedef typename Grid::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGVertexLayout> VertexMapper;
    typedef typename Grid::template Codim<0>::Entity::EntitySeed EntitySeed;


public:

    //public types
    // vector of element seeds
    typedef std::vector<EntitySeed> NeighborElementSeeds;
    // iterator for the neighbor element pointer
    typedef typename NeighborElementSeeds::const_iterator NeighborElementSeedsIterator;
    // map containing vertex index as key and a vector neighbor element pointers as values
    typedef std::unordered_map<int, NeighborElementSeeds > VertexElementMapper;

    VertIdxToElemNeighborMapper(const GridView& gridView)
    : gridView_(gridView), vertexMapper_(gridView)

    {
        //Call the update function in the constructor
        update();
    }

    // reference to the neighbor elements of
    const NeighborElementSeeds& vertexElements(int vIdxGlobal) const
    {
        return vertexElementMapper_.find(vIdxGlobal)->second;
    }

    //return begin iterator
    NeighborElementSeedsIterator vertexElementsBegin(int vIdxGlobal) const
    {
        return vertexElementMapper_.find(vIdxGlobal)->second.begin();
    }

    //return end iterator
    NeighborElementSeedsIterator vertexElementsEnd(int vIdxGlobal) const
    {
        return vertexElementMapper_.find(vIdxGlobal)->second.end();
    }

    // return the reference to the mapper
    const VertexElementMapper& map() const
    {
        return vertexElementMapper_;
    }

    // return map size (number of nodes)
    unsigned int size () const
    {
        return vertexElementMapper_.size();
    }

    // return number of neighbor elements for a global vertex
    unsigned int size (int vIdxGlobal) const
    {
        return vertexElementMapper_.find(vIdxGlobal)->second.size();
    }


    void update()
    {
        vertexMapper_.update();
        vertexElementMapper_.clear();


        ElementIterator eEndIt = gridView_.template end<0>();
        //Loop over elements
        for (ElementIterator eIt = gridView_.template begin<0>(); eIt != eEndIt; ++eIt)
        {
            //get the number of vertices in the element
            int noCorner = eIt->geometry().corners();
            //create and store the entity seed which uses as little memeory as possible
            EntitySeed entitySeed = eIt->seed();
            //Loop over local nodes in element
            for (int vIdx = 0; vIdx < noCorner; ++vIdx)
            {
                //Get the global vertex index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                int vIdxGlobal = vertexMapper_.subIndex(*eIt, vIdx, dim);
#else
                int vIdxGlobal = vertexMapper_.map(*eIt, vIdx, dim);
#endif
                typename VertexElementMapper::iterator vHandle = vertexElementMapper_.find(vIdxGlobal);
                //   if vertex (number) was not found yet, insert new vertex entry with first element neighbor
                if (vHandle == vertexElementMapper_.end())
                {
                    vertexElementMapper_.insert(std::make_pair(vIdxGlobal, NeighborElementSeeds(1, entitySeed)));
                }
                // else if vertex index was already found, insert another element neighbor to that vertex
                else
                {
                    vHandle->second.push_back(entitySeed);
                }
            }
        }
    }

protected:

    const GridView& gridView_;
    VertexElementMapper vertexElementMapper_;
    //vertex map
    VertexMapper vertexMapper_;

};

}

#endif
