## Summary

This is the DuMuX module containing the code for producing the results
published in:

Scholz, Simon
BSc-Thesis 2014


## Installation
The easiest way to install this module is to create a new folder and to execute the file
[installScholz2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2014a/raw/2fc95f5068cf205d2568301fac375a560c0e6b56/Scholz2014a/installScholz2014a.sh)
in this folder.
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


## Applications

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/co2brim/twolayer_generic/,
  appl/co2/co2brim/multilayer_generic/,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/co2brim/twolayer_generic/twolayer_generic.cc,
  appl/co2/co2brim/multilayer_generic/multilayer_generic.cc,

have been extracted.


## Used Versions and Software

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installScholz2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2014a/raw/2fc95f5068cf205d2568301fac375a560c0e6b56/Scholz2014a/installScholz2014a.sh).

There are no additional external software package necessary for
compiling the executables. However, there is a change to the opts-file. See last line of installScholz2014a.sh.
